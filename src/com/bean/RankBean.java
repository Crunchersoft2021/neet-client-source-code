/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.util.ArrayList;

/**
 *
 * @author 007
 */
public class RankBean {

    int unitTestId, rollNo, subject_Id, total_Questions,correct_Questions,incorrect_Questions,testbeanId;
    String timeinstring;
    double ObtainMark,TotalMark,PHY_CORRECT_QUESTIONS,CHEM_CORRECT_QUESTIONS,MATH_CORRECT_QUESTIONS,PHY_INCORRECT_QUESTIONS,
    CHEM_INCORRECT_QUESTIONS,MATH_INCORRECT_QUESTIONS,TOTAL_PHY_QUESTIONS,TOTAL_CHEM_QUESTIONS,
    TOTAL_MATH_QUESTIONS,TOTAL_PHY_MARK,TOTAL_CHEM_MARK,TOTAL_MATH_MARK;

    
    public RankBean() {
    }

    public RankBean(int unitTestId, int rollNo, int subject_Id, int total_Questions, int correct_Questions, int pmark, int nmark, ArrayList<String> subnames,int inco,int testbeanId,String timeinstringr) {
        this.unitTestId = unitTestId;
        this.rollNo = rollNo;
        this.timeinstring=timeinstring;
        this.subject_Id = subject_Id;
        this.total_Questions = total_Questions;
        this.correct_Questions = correct_Questions;
        this.incorrect_Questions = inco;
        this.testbeanId=testbeanId;
    }

    public int getCorrect_Questions() {
        return correct_Questions;
    }

    public void setCorrect_Questions(int correct_Questions) {
        this.correct_Questions = correct_Questions;
    }

    public int getIncorrect_Questions() {
        return incorrect_Questions;
    }

    public String getTimeinstring() {
        return timeinstring;
    }

    public void setTimeinstring(String timeinstring) {
        this.timeinstring = timeinstring;
    }

    public void setIncorrect_Questions(int incorrect_Questions) {
        this.incorrect_Questions = incorrect_Questions;
    }

    public int getTestbeanId() {
        return testbeanId;
    }

    public void setTestbeanId(int testbeanId) {
        this.testbeanId = testbeanId;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public int getSubject_Id() {
        return subject_Id;
    }

    public void setSubject_Id(int subject_Id) {
        this.subject_Id = subject_Id;
    }

    public int getTotal_Questions() {
        return total_Questions;
    }

    public void setTotal_Questions(int total_Questions) {
        this.total_Questions = total_Questions;
    }

    public int getUnitTestId() {
        return unitTestId;
    }

    public void setUnitTestId(int unitTestId) {
        this.unitTestId = unitTestId;
    }
    
    public double getObtainMark() {
        return ObtainMark;
    }

    public void setObtainMark(double ObtainMark) {
        this.ObtainMark = ObtainMark;
    }
    
    public double getTotalMark() {
        return TotalMark;
    }

    public void setTotalMark(double TotalMark) {
        this.TotalMark = TotalMark;
    }
    
    
    public double getPHY_CORRECT_QUESTIONS() {
        return PHY_CORRECT_QUESTIONS;
    }

    public void setPHY_CORRECT_QUESTIONS(double PHY_CORRECT_QUESTIONS) {
        this.PHY_CORRECT_QUESTIONS = PHY_CORRECT_QUESTIONS;
    }

    public double getCHEM_CORRECT_QUESTIONS() {
        return CHEM_CORRECT_QUESTIONS;
    }

    public void setCHEM_CORRECT_QUESTIONS(double CHEM_CORRECT_QUESTIONS) {
        this.CHEM_CORRECT_QUESTIONS = CHEM_CORRECT_QUESTIONS;
    }

    public double getMATH_CORRECT_QUESTIONS() {
        return MATH_CORRECT_QUESTIONS;
    }

    public void setMATH_CORRECT_QUESTIONS(double MATH_CORRECT_QUESTIONS) {
        this.MATH_CORRECT_QUESTIONS = MATH_CORRECT_QUESTIONS;
    }

    public double getPHY_INCORRECT_QUESTIONS() {
        return PHY_INCORRECT_QUESTIONS;
    }

    public void setPHY_INCORRECT_QUESTIONS(double PHY_INCORRECT_QUESTIONS) {
        this.PHY_INCORRECT_QUESTIONS = PHY_INCORRECT_QUESTIONS;
    }

    public double getCHEM_INCORRECT_QUESTIONS() {
        return CHEM_INCORRECT_QUESTIONS;
    }

    public void setCHEM_INCORRECT_QUESTIONS(double CHEM_INCORRECT_QUESTIONS) {
        this.CHEM_INCORRECT_QUESTIONS = CHEM_INCORRECT_QUESTIONS;
    }

    public double getMATH_INCORRECT_QUESTIONS() {
        return MATH_INCORRECT_QUESTIONS;
    }

    public void setMATH_INCORRECT_QUESTIONS(double MATH_INCORRECT_QUESTIONS) {
        this.MATH_INCORRECT_QUESTIONS = MATH_INCORRECT_QUESTIONS;
    }

    public double getTOTAL_PHY_QUESTIONS() {
        return TOTAL_PHY_QUESTIONS;
    }

    public void setTOTAL_PHY_QUESTIONS(double TOTAL_PHY_QUESTIONS) {
        this.TOTAL_PHY_QUESTIONS = TOTAL_PHY_QUESTIONS;
    }

    public double getTOTAL_CHEM_QUESTIONS() {
        return TOTAL_CHEM_QUESTIONS;
    }

    public void setTOTAL_CHEM_QUESTIONS(double TOTAL_CHEM_QUESTIONS) {
        this.TOTAL_CHEM_QUESTIONS = TOTAL_CHEM_QUESTIONS;
    }

    public double getTOTAL_MATH_QUESTIONS() {
        return TOTAL_MATH_QUESTIONS;
    }

    public void setTOTAL_MATH_QUESTIONS(double TOTAL_MATH_QUESTIONS) {
        this.TOTAL_MATH_QUESTIONS = TOTAL_MATH_QUESTIONS;
    }

    public double getTOTAL_PHY_MARK() {
        return TOTAL_PHY_MARK;
    }

    public void setTOTAL_PHY_MARK(double TOTAL_PHY_MARK) {
        this.TOTAL_PHY_MARK = TOTAL_PHY_MARK;
    }

    public double getTOTAL_CHEM_MARK() {
        return TOTAL_CHEM_MARK;
    }

    public void setTOTAL_CHEM_MARK(double TOTAL_CHEM_MARK) {
        this.TOTAL_CHEM_MARK = TOTAL_CHEM_MARK;
    }

    public double getTOTAL_MATH_MARK() {
        return TOTAL_MATH_MARK;
    }

    public void setTOTAL_MATH_MARK(double TOTAL_MATH_MARK) {
        this.TOTAL_MATH_MARK = TOTAL_MATH_MARK;
    }
}
