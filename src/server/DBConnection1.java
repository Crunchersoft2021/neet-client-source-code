/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author 007
 */
public class DBConnection1 {

    public String getURL(String serverIP) {
        int NETWORKSERVER_PORT = 1671;
        String DERBY_CLIENT_URL = "jdbc:derby://" + serverIP + ":" + NETWORKSERVER_PORT+"/";

        return DERBY_CLIENT_URL;
    }

    public Connection getClientConnection1(String serverIP) {
        // Load the JDBC Driver
        int NETWORKSERVER_PORT = 1671;
        String databaseName = "MERGEDB;";
         Connection conn=null;
        String DERBY_CLIENT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
        String DERBY_CLIENT_URL = "jdbc:derby://" + serverIP + ":" + NETWORKSERVER_PORT + "/" + databaseName;
        try {
            Class.forName(DERBY_CLIENT_DRIVER).newInstance();
        } catch (Exception e) {
            System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
            e.printStackTrace();
            System.exit(1);  //critical error, so exit
        }


        // Get database connection via DriverManager api
        try {
//                    Connection conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL, properties);
            conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL, "root", "root");
            System.out.println("Connection request Successful ");
            return conn;
        } catch (Exception e) {
            return null;
//            System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
//            System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
//            e.printStackTrace();
//            System.exit(1);  //critical error, so exit
        }
    }

    public Connection getClientConnection2(String serverIP) {
        // Load the JDBC Driver
        int NETWORKSERVER_PORT = 1672;
        String databaseName = "MERGEDB;";
        String DERBY_CLIENT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
        String DERBY_CLIENT_URL = "jdbc:derby://" + serverIP + ":" + NETWORKSERVER_PORT + "/" + databaseName;
        try {
            Class.forName(DERBY_CLIENT_DRIVER).newInstance();
        } catch (Exception e) {
            System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
            e.printStackTrace();
            System.exit(1);  //critical error, so exit
        }

        // Get database connection via DriverManager api
        try {
//                    Connection conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL, properties);
            Connection conn = (Connection) DriverManager.getConnection(DERBY_CLIENT_URL, "root", "root");
            System.out.println("Connection request Successful ");
            return conn;
        } catch (Exception e) {
            System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
            System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
            e.printStackTrace();
            System.exit(1);  //critical error, so exit
        }

        return null;
    }
    
    
    
//    public Connection getEmbeddedConnection1(){
//                // Load the JDBC Driver
//                String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
//                String DERBY_EMBEDDED_URL="jdbc:derby:CETJeeNew;dataEncryption=true;bootPassword=*007_Cruncher+Soft@Wagholi_007*";
//		try{
//                    Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
//		} 
//                catch (Exception e) {
//                    System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
//                    e.printStackTrace();
//                    System.exit(1);  //critical error, so exit
//		}
//		
//		Properties properties = new java.util.Properties();		
//		properties.setProperty("user","derbyuser");
//		properties.setProperty("password","pass");
//
//		// Get database connection via DriverManager api
//		try{			
//                    Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL);
//                    System.out.println("Embedded Connection request Successful ");
//                    return conn;
//		} 
//                catch(Exception e){
//			System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
//			System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
//			e.printStackTrace();
//			System.exit(1);  //critical error, so exit
//		  }
//
//		return null;
//        }          
//    
//    public Connection getEmbeddedConnection2(){
//                // Load the JDBC Driver
//                String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
//                String DERBY_EMBEDDED_URL="jdbc:derby:CETNeetNew;dataEncryption=true;bootPassword=*007_Worlds+Best+Company@Wagholi_007*";
//		try{
//                    Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
//		} 
//                catch (Exception e) {
//                    System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
//                    e.printStackTrace();
//                    System.exit(1);  //critical error, so exit
//		}
//		
//		Properties properties = new java.util.Properties();		
//		properties.setProperty("user","derbyuser");
//		properties.setProperty("password","pass");
//
//		// Get database connection via DriverManager api
//		try{			
//                    Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL);
//                    System.out.println("Embedded Connection request Successful ");
//                    return conn;
//		} 
//                catch(Exception e){
//			System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
//			System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
//			e.printStackTrace();
//			System.exit(1);  //critical error, so exit
//		  }
//
//		return null;
//        }          
//    
}