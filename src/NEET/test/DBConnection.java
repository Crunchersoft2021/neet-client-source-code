package NEET.test;

import NEET.unitTest.UnitTestBean;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.*;
import NEET.question.QuestionBean;
import server.DBConnection1;
import server.Server;

public class DBConnection {
    
//    private static  String driver = "org.apache.derby.jdbc.EmbeddedDriver";
//    private static  String protocol = "jdbc:derby:CETNeetNew;create=true;dataEncryption=true;bootPassword=*007_Worlds+Best+Company@Wagholi_007*";
    public int chaptercount;  
    public ArrayList<Integer> E=new ArrayList<Integer>();
    
    public static void delAllTest(int testId) 
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        try
        {
            String sql="delete from Saved_All_Test where savedId=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public static void delTest(int testId) 
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        try
        {
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
      
    public int getPerQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();            
            rs = s.executeQuery("Select Marks_Per_Question from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(1);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public int getPerwrongQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();           
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(1);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }    
    
    public ArrayList<String> nameOfChap(int sub_id)
    {
        ArrayList<String> al=new ArrayList<String>();
        String cname;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();             
            rs1 = s.executeQuery("select Chapter_Name from chapter_info where subject_id ="+sub_id);
            while(rs1.next())
            {
                cname=rs1.getString(1);
                al.add(cname);
            }
            return al;
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Error");
        }
        return null;
    }
    
    public int noOfChap(int sub_id)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();           
            rs1 = s.executeQuery("select count(chapter_id) from chapter_info where subject_id ="+sub_id);           
            while(rs1.next())
            {
                chaptercount=rs1.getInt(1);
            }
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return chaptercount;
    }
    
    public void createTables()
    {        
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();                       
            s.execute("CREATE TABLE Test_Info (test_Id INTEGER NOT NULL,group_Id INTEGER NOT NULL,status VARCHAR(30) NOT NULL,start_DateTime TIMESTAMP NOT NULL,PRIMARY KEY (test_Id))");
            s.execute("CREATE TABLE Group_Info (group_Id INTEGER NOT NULL,group_Name VARCHAR(45) NOT NULL,physics_Question_Count INTEGER NOT NULL,Chemistry_Question_Count INTEGER NOT NULL,Maths_Question_Count INTEGER NOT NULL,Biology_Question_Count INTEGER NOT NULL,Total_Minutes INTEGER NOT NULL,PRIMARY KEY (group_Id))");            
            s.execute("CREATE TABLE Test_Question_Status_Info (test_Id INTEGER NOT NULL,question_Id INTEGER NOT NULL,user_Answer VARCHAR(30) NOT NULL)");            
            s.execute("CREATE TABLE Test_Result_Info (test_Id INTEGER NOT NULL,subject_Id INTEGER NOT NULL,total_Questions INTEGER NOT NULL,correct_Questions INTEGER NOT NULL,PRIMARY KEY (test_Id,subject_Id))");

            
            // s.execute("DROP table Practice_Info");
            // s.execute("DROP table Practice_Question_Status_Info");
//             s.execute("DROP table Practice_Result_Info");
            //            
             s.execute("CREATE TABLE Practice_Info (practice_Id INTEGER NOT NULL,subject_Id INTEGER NOT NULL,chapter_Id INTEGER NOT NULL,status VARCHAR(30) NOT NULL,start_DateTime TIMESTAMP NOT NULL,PRIMARY KEY (practice_Id))");
             s.execute("CREATE TABLE Practice_Question_Status_Info (practice_Id INTEGER NOT NULL,question_Id INTEGER NOT NULL,user_Answer VARCHAR(30) NOT NULL)");
             s.execute("CREATE TABLE Practice_Result_Info (practice_Id INTEGER NOT NULL,total_Questions INTEGER NOT NULL,correct_Questions INTEGER NOT NULL,PRIMARY KEY (practice_Id))");
            
            // "SELECT "+ mathsObtained +"FROM Result_Info WHERE "+  mathsTotal +" > 0 AND test_Id IN (SELECT test_Id FROM test_info WHERE group_Id="++")";              
            //System.out.println("Created tables");
            
            
//            rs1 = s.executeQuery("Select * from Practice_Result_Info ");          
//            while(rs1.next())
//            {                
//                //System.out.println(rs1.getInt(1)+" "+rs1.getInt(2)+" "rs1.getInt(3)+" "rs1.getInt(4)+" ");                 
//                System.out.println(rs1.getInt(1)+" "+rs1.getInt(2)+" "+rs1.getInt(3));//+" "+rs1.getInt(4));
//            }
//            
//            rs1 = s.executeQuery("Select count(*) from Practice_Info ");          
//            while(rs1.next())
//            {                
//                //System.out.println(rs1.getInt(1)+" "+rs1.getInt(2)+" "rs1.getInt(3)+" "rs1.getInt(4)+" ");                 
//                System.out.println(rs1.getInt(1));
//            }
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void resetQuestionAttempt(int subjectId,int questionCount)
    {
        int count=0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();                     
            rs1 = s.executeQuery("Select count(*) from Question_Info where Subject_Id ="+subjectId+"and  AttemptedValue = 0");          
            if(rs1.next())
            {                
                count = rs1.getInt(1);                 
            }
            if(count<questionCount)
            {
                s.executeUpdate("Update Question_Info set AttemptedValue = 0 where Subject_Id ="+subjectId);
            }
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
        
    public ArrayList<QuestionBean> selectQuestionsForTest(int subjectId,int questionCount)
    {        
        resetQuestionAttempt(subjectId, questionCount);        
        ArrayList<QuestionBean> al = getQuestionsForTest(subjectId);
        if(al!=null)
        {             
            Collections.shuffle(al);
            ArrayList<QuestionBean> alFinal=new ArrayList<QuestionBean>();
            for (int idx = 0; idx < questionCount; ++idx)
            {                
                alFinal.add(al.get(idx));
            }
            return alFinal;
        }
        return null;
    }
    
    public ArrayList<Integer> getQuestionCount(int groupId)
    {
        ArrayList<Integer> al = new ArrayList<Integer>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();            
            rs = s.executeQuery("Select * from Group_Info where Group_Id = "+groupId);           
            if(rs.next())
            {                
                al.add(rs.getInt(3));
                al.add(rs.getInt(4));
                al.add(rs.getInt(5));
                al.add(rs.getInt(6));
            }
            return al;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
        
    public ArrayList<QuestionBean> getchapno(ArrayList<String> nameofchap)
    {
        ArrayList<Integer> noofchaps = null;
        ArrayList<QuestionBean> questionBean1=getQuestionsForUnitTest(noofchaps);
        return questionBean1;
    }
    
    public TestBean getTest(int groupId)
    {
        int physicsCount=0,chemistryCount=0,bioCount=0,totalTime=0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            String query = "Select * from Group_info where group_Id = "+groupId;
            rs=s.executeQuery(query);
            if(rs.next())
            {
                physicsCount = rs.getInt(3);
                chemistryCount = rs.getInt(4);
                bioCount = rs.getInt(5);
                totalTime = rs.getInt(6);
            }
            
            ArrayList<QuestionBean> alPhysics=selectQuestionsForTest(1, physicsCount);
            ArrayList<QuestionBean> alChemistry=selectQuestionsForTest(2, chemistryCount);
            ArrayList<QuestionBean> alBio=selectQuestionsForTest(3, bioCount);
            TestBean testBean = new TestBean();
            testBean.setGroupId(groupId);
            testBean.setTotalTime(totalTime);
            testBean.setRemainingTime(totalTime);
            testBean.setAlPhysics(alPhysics);
            testBean.setAlChemistry(alChemistry);
            testBean.setAlBiology(alBio);
            
            return testBean;
            
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<TestResultForGraph> getResultObjects(int rollNo)
    {
        ArrayList<TestResultForGraph> testResultForGraphs=new ArrayList<TestResultForGraph>();
        int count = 0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            String query1 = "Select count(*) from Test_Result_Info where rollNo="+rollNo;
            rs=s.executeQuery(query1);
            if(rs.next())
            {
                count=rs.getInt(1);
            }
            if(count>10){
                count=count-10;
            }
            else{
                count=0;
            }
            String query = "Select * from Test_Result_Info where rollNo="+rollNo+" order by test_Id";
            rs=s.executeQuery(query);
            int i=0;
            while(rs.next())
            {
                if(i>=count){
                    TestResultForGraph testResult=new TestResultForGraph();
                    testResult.setTest_Id(rs.getInt(1));
                    testResult.setSubject_Id(rs.getInt(3));
                    testResult.setTotal_Questions(rs.getInt(4));
                    testResult.setCorrect_Questions(rs.getInt(5));
                    testResultForGraphs.add(testResult);
                }
                i++;
            }
            return testResultForGraphs;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testResultForGraphs;
    }
    
    public ArrayList<Integer> getSubjectResultData(int subjectId, int rollNo)
    {
        int count=0;
        ArrayList<Integer> al = new ArrayList<Integer>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try{  
            Statement s=conn.createStatement();          
            String query2 = "Select count(*) from Test_Result_Info where subject_Id = "+subjectId+" and rollNo="+rollNo;
            rs=s.executeQuery(query2);
            if(rs.next())
            {
                count=rs.getInt(1);
            }
            if(count>10){
                count=count-10;
            }
            else{
                count=0;
            }
            int i=0;
            String query = "Select * from Test_Result_Info where subject_Id = "+subjectId+" and rollNo="+rollNo+" order by test_Id";
            rs=s.executeQuery(query);
            while(rs.next()){
                if(i>=count){
                    al.add(rs.getInt(5));
                }
                i++;
            }            
            return al;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public Integer getMaxTestResId()
    {        
        int id=0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            String query = "Select max(test_Id) from Test_Result_Info";
            rs=s.executeQuery(query);
            if(rs.next())
            {
                id=rs.getInt(1);
            }
            return id;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return id;
    }
    
    public ArrayList<Object[]> getSubjectList()
    { 
        ArrayList<Object[]> subjects=new ArrayList<Object[]>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();           
            rs = s.executeQuery("Select * from Subject_Info");           
            while(rs.next())
            {                
                subjects.add(new Object[]{rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4)});
            }
            return subjects;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public void saveSubjects(java.util.Vector rows)
    {
        int testId=0;
        if(rows!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();                  
                int size = rows.size();
                PreparedStatement ps=conn.prepareStatement("Update Subject_Info set Marks_Per_Question = ?,Marks_Per_Wrong_Question = ? where Subject_Id = ?");
                for(int i=0;i<size;i++)
                {
                    java.util.Vector row = (java.util.Vector) rows.elementAt(i);
                    int subjectId = (Integer) row.elementAt(0);
                    String subjectName = (String) row.elementAt(1);
                    int subjectMark = (Integer) row.elementAt(2);
                    int negativesubjectMark = (Integer) row.elementAt(3);
                    
                    ps.setInt(1, subjectMark); 
                    ps.setInt(3, subjectId);       
                    ps.setInt(2, negativesubjectMark);       
                    
                    int r = ps.executeUpdate();
                    //System.out.println(r);
                }
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
        
    public LinkedHashMap<String,Integer> getChapterList(int Subject_Id)
    { 
        LinkedHashMap<String,Integer> chapters=new LinkedHashMap<String,Integer>();
        ArrayList<String> al=new ArrayList<String>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            rs1 = s.executeQuery("Select * from Chapter_Info where Subject_Id="+Subject_Id);           
            while(rs1.next())
            {                
                chapters.put(rs1.getString(3),rs1.getInt(1));
            }
            return chapters;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public ArrayList<Object[]> getGroupList()
    { 
        ArrayList<Object[]> groups=new ArrayList<Object[]>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            rs = s.executeQuery("Select * from Group_Info");           
            while(rs.next())
            {                
                groups.add(new Object[]{rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6)});
            }
            return groups;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<Object[]> getsummary()
    { 
        ArrayList<Object[]> summ=new ArrayList<Object[]>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs,rs1;
        try
        {  
            Statement s=conn.createStatement();          
            rs = s.executeQuery("Select * from Test_Info");    
            rs1= s.executeQuery("Select * from Test_Result_Info");    
            while(rs.next())
            {                
                int i=0;
                String grp = "-";
                i=rs.getInt(3);
                if(i==1)
                {
                    grp="Physics";
                }
                else if(i==2)
                {
                    grp="Chemistry";
                }
                else if(i==3)
                {
                    grp="Biology";
                }
                else if(i==4)
                {
                    grp="PCB";
                }
                else
                {
                    grp="-";
                }
                summ.add(new Object[]{rs.getInt(1),grp,rs.getTimestamp(5),rs1.getInt(4),rs1.getInt(5)});//changed
            }
            return summ;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public int saveGroups(java.util.Vector rows)
    {
        if(rows!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                int size = rows.size();
                PreparedStatement ps=conn.prepareStatement("Update Group_Info set physics_Question_Count=?,Chemistry_Question_Count=?,Biology_Question_Count=?,Total_Minutes=? where group_Id = ?");
                for(int i=0;i<size;i++)
                {
                    java.util.Vector row = (java.util.Vector) rows.elementAt(i);
                    int pc = (Integer) row.elementAt(2);
                    int cc = (Integer) row.elementAt(3);
                    int bc = (Integer) row.elementAt(4);
                    int time = (Integer) row.elementAt(5);
                    int groupId = (Integer) row.elementAt(0);
                    
                    
                    ps.setInt(5, groupId); 
                    ps.setInt(1, pc); 
                    ps.setInt(2, cc);//(Integer)row.elementAt(3)); 
                    ps.setInt(3, bc);//(Integer)row.elementAt(5)); 
                    ps.setInt(4, time);//(Integer)row.elementAt(6));
                    int r = ps.executeUpdate();
                }
                return 1;
            } 
            catch (SQLException ex) 
            {
                ex.printStackTrace();
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
        return 0;
    }
    
    private ArrayList<QuestionBean> getQuestionsForTest(int Subject_Id)
    {                    
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            ArrayList<QuestionBean> al=new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id ="+Subject_Id+"and  AttemptedValue = 0 order by Question_Id");          
            while(rs1.next())
            {
                String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,HintImagePath;
                int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                question_Id=rs1.getInt(1);
                question=rs1.getString(2);
                A=rs1.getString(3);
                B=rs1.getString(4);
                C=rs1.getString(5);
                D=rs1.getString(6);
                Answer=rs1.getString(7);
                Hint=rs1.getString(8);                
                sub_Id=rs1.getInt(9);
                Chap_Id=rs1.getInt(10);
                Top_Id=rs1.getInt(11);
                QuestionImagePath=rs1.getString(13);
                HintImagePath=rs1.getString(17);
                optionImagePath=rs1.getString(15);
                boolean result = (rs1.getInt(12)==0)?false:true;
                boolean result1 = (rs1.getInt(14)==0)?false:true;               
                boolean result2 = (rs1.getInt(16)==0)?false:true;
                attempt=rs1.getInt(18);
                QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, HintImagePath,attempt);
                al.add(q);    
            }    
            return al;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private ArrayList<QuestionBean> getQuestionsForUnitTest(ArrayList<Integer> noofchaps)
    {       
        int size=noofchaps.size();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            ArrayList<QuestionBean> al=new ArrayList<QuestionBean>();
            for(int i=0;i<size;i++)
            {
                int m=0;
                while(m!=10)
                {
                    rs1 = s.executeQuery("Select distinct * from Question_Info where Chapter_Id ="+noofchaps.get(i) +"and  AttemptedValue = 0 order by Question_Id");          
                    while(rs1.next())
                    {
                        String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,HintImagePath;
                        int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                        question_Id=rs1.getInt(1);
                        question=rs1.getString(2);
                        A=rs1.getString(3);
                        B=rs1.getString(4);
                        C=rs1.getString(5);
                        D=rs1.getString(6);
                        Answer=rs1.getString(7);
                        Hint=rs1.getString(8);                
                        sub_Id=rs1.getInt(9);
                        Chap_Id=rs1.getInt(10);
                        Top_Id=rs1.getInt(11);
                        QuestionImagePath=rs1.getString(13);
                        HintImagePath=rs1.getString(17);
                        optionImagePath=rs1.getString(15);
                        boolean result = (rs1.getInt(12)==0)?false:true;
                        boolean result1 = (rs1.getInt(14)==0)?false:true;               
                        boolean result2 = (rs1.getInt(16)==0)?false:true;
                        attempt=rs1.getInt(18);
                        QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, HintImagePath,attempt);
                        al.add(q);    
                    }
                    m++;
                }
            }
            return al;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
       
    public ArrayList<QuestionBean> selectQuestionsForPractice(int subjectId,int questionCount)
    {        
        ArrayList<QuestionBean> al=getQuestionsForPractice(subjectId);        
        if(al!=null)
        {
            ArrayList<QuestionBean> alFinal=new ArrayList<QuestionBean>();
            int size = al.size();
            Random randomGenerator = new Random();
            for (int idx = 1; idx <= questionCount; ++idx)
            {
                int randomInt = randomGenerator.nextInt(size);
                alFinal.add(al.get(randomInt));
            }
            return alFinal;
        }
        return null;
    }
    
    public void finishTest(int testId)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            int r = s.executeUpdate("Update Test_Info set status='Finished' where testId="+testId);
            if(r>0)
            {
                //System.out.println("Test finished");
            }
        }
        catch(Exception e)
        {
            
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void saveStateOfNewTest(TestBean testBean,int rollNo)
    {
        int testId=0;
        if(testBean!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                rs = s.executeQuery("Select max(test_Id) from Test_Info");
                if(rs.next())
                {
                        testId=rs.getInt(1);
                }
                testId++;
                
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate=formatter.format(date);
         
                PreparedStatement ps=conn.prepareStatement("insert into Test_Info values(?,?,?,?,?)");
                ps.setInt(1, testId);
                ps.setInt(2, rollNo);
                ps.setInt(3, testBean.getGroupId());
                ps.setString(4,"Started");                
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                ps.executeUpdate();
                
                testBean.setTestId(testId);
                
                ArrayList<QuestionBean> alPhysics=testBean.getAlPhysics();
                ArrayList<QuestionBean> alChemistry=testBean.getAlChemistry();
                ArrayList<QuestionBean> alBiology=testBean.getAlBiology();
                               
                Iterator<QuestionBean> it = alPhysics.iterator();
                while(it.hasNext())
                {
                    QuestionBean question = it.next();                    
                    ps=conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestion_Id());
                    ps.setString(3,"unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id="+question.getQuestion_Id());
                }
                
                it = alChemistry.iterator();
                while(it.hasNext())
                {
                    QuestionBean question = it.next();                    
                    ps=conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestion_Id());
                    ps.setString(3,"unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id="+question.getQuestion_Id());
                }

                it = alBiology.iterator();
                while(it.hasNext())
                {
                    QuestionBean question = it.next();                    
                    ps=conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestion_Id());
                    ps.setString(3,"unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id="+question.getQuestion_Id());
                }
                testBean.setStatus("Started");     
                testBean.setStartDateTime(new java.util.Date());
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public void setQuestionStatus(QuestionBean question,int testId, String userAnswer)
    {
        if(question!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                s.executeUpdate("update Test_Question_Status_Info set user_Answer='"+userAnswer+"' where Question_Id="+question.getQuestion_Id()+" and test_Id="+testId);                               
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public String getResult(int testId,int questionId)
    {        
           Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                rs = s.executeQuery("Select user_Answer from  Test_Question_Status_Info where test_Id="+testId+" and Question_Id="+questionId);   
                if(rs.next())
                {
                    return rs.getString(1);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
            return null;
    }
    
    private ArrayList<QuestionBean> getQuestionsForPractice(int Subject_Id)
    {                    
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            ArrayList<QuestionBean> al=new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select * from Question_Info where Subject_Id ="+Subject_Id+" order by Question_Id");          
            while(rs1.next())
            {
                String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,hintImagePath;
                int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                question_Id=rs1.getInt(1);
                question=rs1.getString(2);
                A=rs1.getString(3);
                B=rs1.getString(4);
                C=rs1.getString(5);
                D=rs1.getString(6);
                Answer=rs1.getString(7);
                Hint=rs1.getString(8);                
                sub_Id=rs1.getInt(9);
                Chap_Id=rs1.getInt(10);
                Top_Id=rs1.getInt(11);
                QuestionImagePath=rs1.getString(13);
                optionImagePath=rs1.getString(15);
                hintImagePath=rs1.getString(17);
                boolean result = (rs1.getInt(12)==0)?false:true;
                boolean result1 = (rs1.getInt(14)==0)?false:true;               
                boolean result2 = (rs1.getInt(16)==0)?false:true;               
                attempt=rs1.getInt(18);
                QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt);
                al.add(q);    
            }    
            return al;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public void addResult(TestResultBean resultBean,int rollNo)
    {
        if(resultBean!=null)
        {            
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                String sql="insert into Test_Result_Info values(?,?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1,resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3,1);
                ps.setInt(4,resultBean.getTotalPhysics());
                ps.setInt(5,resultBean.getObtainedPhysics());
                int r = ps.executeUpdate();                
                
                ps.setInt(1,resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3,2);
                ps.setInt(4,resultBean.getTotalChemistry());
                ps.setInt(5,resultBean.getObtainedChemistry());
                r = ps.executeUpdate();   
                
                
                ps.setInt(1,resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3,3);
                ps.setInt(4,resultBean.getTotalBiology());
                ps.setInt(5,resultBean.getObtainedBiology());
                r = ps.executeUpdate();
                
                if(r>0)
                {
                    //System.out.println("Result Added");
                }
                else
                {
                    //System.out.println("Problem in Result Adding");
                }
            } 
            catch (Exception ex) 
            {
                ex.printStackTrace();
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public void viewXYAreaGraph(int groupId,int rollNo)
    {
        XYSeries series = new XYSeries("Average Weight");
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            String subjectObtained="",subjectTotal="";
            switch(groupId)
            {
                case 1 : subjectObtained = "PhysicsObtained"; subjectTotal = "PhysicsTotal"; break;
                case 2 : subjectObtained = "ChemistryObtained"; subjectTotal = "ChemistryTotal"; break;
                case 3 : subjectObtained = "BiologyObtained"; subjectTotal = "BiologyTotal"; break;
            }
            String query = "SELECT "+ subjectObtained +" FROM Test_Result_Info WHERE "+  subjectTotal +" > 0 AND test_Id IN (SELECT distinct test_Id FROM test_info WHERE group_Id="+groupId+" and rollNo = "+rollNo+") order by test_Id";
            rs1 = s.executeQuery(query);     
            int i=1;
            while(rs1.next())
            {
                series.add(i, rs1.getInt(1));
                i++;
            }
            XYDataset xyDataset = new XYSeriesCollection(series);
                JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart using JFreeChart", "Age", "Weight",xyDataset, PlotOrientation.VERTICAL, true,
        true, false);
                ChartFrame frame1=new ChartFrame("XYArea Chart",chart);
                frame1.setVisible(true);
                frame1.setSize(300,300);
        }   
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
        
    public static void main(String s1[])
    {
        DBConnection s=new DBConnection();
        s.createTables();
    }

    public UnitTestBean getClassTestBean(int testId) {
        UnitTestBean unitTestBean=new UnitTestBean();
        String flag = null;
        ArrayList<Integer> queIds=new ArrayList<Integer>();
        ArrayList<QuestionBean> al=new ArrayList<QuestionBean>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs1;
            try
            {  
                Statement s=conn.createStatement();
                rs1 = s.executeQuery("Select status from Class_Test_Info where test_Id="+testId);
                if(rs1.next())
                {
                        flag=rs1.getString(1);
                }
                rs1 = s.executeQuery("Select question_Id from Class_Test_Question_Info where testId="+testId);
                while(rs1.next())
                {                       
                    int q=rs1.getInt(1);
                    queIds.add(q);
                }
                for(int i=0;i<queIds.size();i++){
                    rs1 = s.executeQuery("Select * from Question_Info where Question_Id="+queIds.get(i));
                    if(rs1.next())
                    {                       
                        String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,hintImagePath;
                        int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                        question_Id=rs1.getInt(1);                            
                        question=rs1.getString(2);
                        A=rs1.getString(3);
                        B=rs1.getString(4);
                        C=rs1.getString(5);
                        D=rs1.getString(6);
                        Answer=rs1.getString(7);
                        Hint=rs1.getString(8);                
                        sub_Id=rs1.getInt(9);
                        Chap_Id=rs1.getInt(10);    
                        Top_Id=rs1.getInt(11);
                        QuestionImagePath=rs1.getString(13);
                        optionImagePath=rs1.getString(15);
                        hintImagePath=rs1.getString(17);
                        boolean result = (rs1.getInt(12)==0)?false:true;
                        boolean result1 = (rs1.getInt(14)==0)?false:true;               
                        boolean result2 = (rs1.getInt(16)==0)?false:true;               
                        attempt=rs1.getInt(18);
                        QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt);
                        al.add(q);    
                    }
                }
                unitTestBean.setQuestions(al);
                unitTestBean.setStatus(flag);
                return unitTestBean;
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return unitTestBean;
    }

    public int getClassTestTime(int testId) {
        int time=0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs1;
            try
            {  
                Statement s=conn.createStatement();
                rs1 = s.executeQuery("Select testTime from Class_Test_Info where test_Id="+testId);
                if(rs1.next())
                {
                        time=rs1.getInt(1);
                }
                return time;
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return time;
    }
}