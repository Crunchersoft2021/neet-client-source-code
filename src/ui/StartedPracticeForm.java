package ui;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.*;
import JEE.practice.PracticeBean;

import JEE.practice.DBConnection;
import JEE.practice.SavePractice;
import JEE.unitTest.UnitTestBean;
import com.bean.QuestionBean;
public class StartedPracticeForm extends javax.swing.JFrame {
    QuestionPanel currentPanel=null;
    ArrayList<QuestionBean> alQuestions;    
    boolean flagSubject=false,isNewTest=true;
    DBConnection db;int type;
    PracticeBean practiceBean;
    private javax.swing.JButton[] jButtonsArray;
    int animationTime=5,sec = 0,min = 0,hour = 0,currentIndex;
    long remaining; // How many milliseconds remain in the countdown.
    long lastUpdate; // When count was last updated  
    Timer timer; // Updates the count every second
    NumberFormat format;
    UnitTestBean uTestBean;
    /** Creates new form StartedTestForm */
      int rollNo;
    /** Creates new form TestResultForm */
    public StartedPracticeForm(int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        setIconImage(new ImageIcon(getClass().getResource("/ui/images/c.gif")).getImage());
        this.setState(JFrame.MAXIMIZED_BOTH);
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        cl.show(jPanelsSliding1, "card4");
        currentPanel=questionPanel1;
        db=new DBConnection();
        btnPause.setText("Pause");      
        btnHint.setVisible(false);
        this.getContentPane().setBackground(Color.white);
    } 
    public void setPanel(boolean flag)
    {
        QuestionBean questionBean = alQuestions.get(currentIndex);
        if( questionBean.getHint().equals("\\mbox{null}") ||questionBean.getHint().equals("\\mbox{ }") ||questionBean.getHint().equals("\\mbox{  }") ||questionBean.getHint().equals("\\mbox{   }") ||questionBean.getHint().equals("\\mbox{\\mbox{}}") ||questionBean.getHint().equals("\\mbox{") ||questionBean.getHint().equals("\\mbox{    }") ||questionBean.getHint().equals("") ||questionBean.getHint() == null||questionBean.getHint().equals("\\mbox{}"))
        {
            btnHint.setVisible(false);
        }
        else
        {
            btnHint.setVisible(true);
        }
        currentPanel=(currentPanel==questionPanel1)?questionPanel2:questionPanel1;
        currentPanel.unlockSelection();    
        //set Question        
        currentPanel.setQuestionOnPanel(alQuestions.get(currentIndex),(currentIndex+1));
        txtQuestionNumber.setText((currentIndex+1)+"");
        //slide panel    
        jPanelsSliding1.nextSlidPanel(animationTime,currentPanel,flag);
        System.out.println("Time : "+animationTime);
        jPanelsSliding1.refresh();
    }
    
    public void setQuestion(String actionCommand)
    {
        //setred();
        currentIndex=Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }
    
    public void setButtonOnPanel(JPanel queButtonPanels)
    {        
        int subid=practiceBean.getSubjectId();
        jButtonsArray=new JButton[10];
        for(int x = 0; x < 10 ; x++){
                jButtonsArray[x] = new javax.swing.JButton();
                jButtonsArray[x].setActionCommand(subid+" "+x);
                if(alQuestions.get(x).getUserAnswer().equals("UnAttempted")){
                    jButtonsArray[x].setBackground(Color.white);
                }
                else{
                    jButtonsArray[x].setBackground(Color.green);
                }
                jButtonsArray[x].setToolTipText("Not Answered");
                jButtonsArray[x].setSize(50,50);
                jButtonsArray[x].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                int y=x+1;
                jButtonsArray[x].setText(""+y);
       }
       GridBagConstraints cons=new GridBagConstraints();
            cons.gridx = 0;   cons.gridy  =  0;
            cons.gridwidth = 1;   cons.gridheight =  1;
            cons.anchor  =  GridBagConstraints.BELOW_BASELINE_LEADING;
            cons.weightx =  2;   cons.weighty =  1;
            cons.insets=new java.awt.Insets(1, 3, 1, 3);
            GridBagLayout layout = new GridBagLayout();
            for(int x=0;x<10;x++)
            {
                layout.setConstraints(jButtonsArray[x], cons);
                queButtonPanels.setLayout(layout);
                queButtonPanels.add(jButtonsArray[x],cons);                
                cons.gridy++;
            }
    }
    
    public void setButtonOnPanel(JPanel queButtonPanels,int que)
    {        
        int subid=uTestBean.getSubjectId();
        que=30;
        jButtonsArray=new JButton[que];
        for(int x = 0; x < que ; x++){
                jButtonsArray[x] = new javax.swing.JButton();
                jButtonsArray[x].setActionCommand(subid+" "+x);
                jButtonsArray[x].setBackground(Color.white);
                jButtonsArray[x].setToolTipText("Not Answered");
                jButtonsArray[x].setSize(50,50);
                jButtonsArray[x].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                int y=x+1;
                jButtonsArray[x].setText(""+y);
       }
       GridBagConstraints cons=new GridBagConstraints();
            cons.gridx = 0;   cons.gridy  =  0;
            cons.gridwidth = 1;   cons.gridheight =  1;
            cons.anchor  =  GridBagConstraints.BELOW_BASELINE_LEADING;
            cons.weightx =  2;   cons.weighty =  1;
            cons.insets=new java.awt.Insets(1, 3, 1, 3);
            GridBagLayout layout = new GridBagLayout();
            for(int x=0;x<que;x++)
            {
                if(x%15==0)
                {
                    cons.gridx++;
                    cons.gridy=1;
                }                
                layout.setConstraints(jButtonsArray[x], cons);
                queButtonPanels.setLayout(layout);
                queButtonPanels.add(jButtonsArray[x],cons);                
                cons.gridy++;
            }
    }
    
    public StartedPracticeForm(UnitTestBean testBean,boolean newTest,boolean newTe,boolean k,int rollNo) {
        initComponents();  
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        type=2;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db=new DBConnection();
        this.uTestBean=testBean;
        currentPanel=questionPanel1;
        ArrayList<Integer> E=new ArrayList<Integer>();
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        currentIndex=uTestBean.getCurrentQuestionNumber();
        alQuestions=uTestBean.getQuestions();
        switch(uTestBean.getSubjectId())
        {
            case 1: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    lblHeading.setText("Unit Test - - ->Physics.");
                    break;
            case 2: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    lblHeading.setText("Unit Test - - ->Chemistry.");
                    break;
            case 3: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    lblHeading.setText("Unit Test - - ->Maths.");
                    break;
        }
        cmbSub.setSelectedIndex(-1);   
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        if(newTest)
        {
            JEE.unitTest.DBConnection df=new JEE.unitTest.DBConnection();
            df.saveStateOfNewPractice(uTestBean,rollNo);
            setButtonOnPanel(pnlAllQue,5);
            int minutes= testBean.getTotalTime();
            //System.out.println(minutes+" min");
            if (minutes > 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            cmbSub.setSelectedIndex(0);
        }
        else
        {
            setButtonOnPanel(pnlAllQue,5);
            
            remaining = testBean.getRemainingTime();  
            isNewTest = false;
            System.out.print(uTestBean.getSubjectId());
            setPanel(true);
//            cmbSub.setSelectedIndex(uTestBean.getSubjectId()-1);
        }
        timer.setInitialDelay(0); 
        timer.start();
    }
    
    public StartedPracticeForm(PracticeBean practiceBean,boolean newTest,int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        type=1;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db=new DBConnection();
        this.practiceBean=practiceBean;
        
        currentPanel=questionPanel1;
        this.alQuestions = practiceBean.getQuestions();        
                
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        //cl.show(jPanelsSliding1, "card4");
        
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        String chapname=db.getChapName(practiceBean.getChapterId());
        switch(practiceBean.getSubjectId())
            {
                case 1: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    lblHeading.setText("Practice - - ->Physics - - ->"+chapname);
                    break;
            case 2: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    lblHeading.setText("Practice - - ->Chemistry - - ->"+chapname);
                    break;
            case 3: cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    lblHeading.setText("Practice - - ->Maths - - ->"+chapname);
                    break;
            }
        if(newTest)
        {
            db.saveStateOfNewPractice(practiceBean,rollNo);
            int minutes= 0;
            //System.out.println(minutes+" min");
            if (minutes == 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            currentIndex=0;
        }
        else
        {
            remaining = practiceBean.getRemainingTime();  
            currentIndex = practiceBean.getCurrentQuestionNumber();
        }
        timer.setInitialDelay(0); 
        timer.start();
        setButtonOnPanel(pnlAllQue);
        setPanel(false);
    }
    public void start() {
    resume();
  } // Start displaying updates
  // The browser calls this to stop the applet. It may be restarted later.
  // The pause() method is defined below
  public void stop() {
    pause();
  }
  // Start or resume the countdown
  void resume() {
    // Restore the time we're counting down from and restart the timer.
    lastUpdate = System.currentTimeMillis();
    timer.start(); // Start the timer
  }
  // Pause the countdown
  void pause() {
    // Subtract elapsed time from the remaining time and stop timing
    long now = System.currentTimeMillis();
    remaining -= (now - lastUpdate);
    timer.stop(); // Stop the timer
  }
  void updateDisplay() {
    long now = System.currentTimeMillis(); // current time in ms
    long elapsed = now - lastUpdate; // ms elapsed since last update
    remaining -= elapsed; // adjust remaining time
    lastUpdate = now; // remember this update time

    // Convert remaining milliseconds to mm:ss format and display
    
      
    int hours   =  (int)(remaining / 3600000);
    hours=hours*(-1);
    int minutes = (int) ((remaining % 3600000) / 60000);
    minutes=minutes*(-1);
    int seconds = (int) ((remaining % 60000) / 1000);
    seconds=seconds*(-1);
    lblTimer.setText(format.format(hours) + ":" +format.format(minutes) + ":" + format.format(seconds));  
  }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnNext = new javax.swing.JButton();
        txtQuestionNumber = new javax.swing.JTextField();
        cmbSub = new javax.swing.JComboBox();
        btnLast = new javax.swing.JButton();
        btnFirst = new javax.swing.JButton();
        btnPrevious = new javax.swing.JButton();
        lblMsg = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rdoEnable = new javax.swing.JRadioButton();
        rdoDisable = new javax.swing.JRadioButton();
        btnSuspend = new javax.swing.JButton();
        btnPause = new javax.swing.JButton();
        lblTimer = new javax.swing.JLabel();
        btnResume = new javax.swing.JButton();
        btnSubmit = new javax.swing.JButton();
        btnEndPractice = new javax.swing.JButton();
        btnHint = new javax.swing.JButton();
        pnlAllQue = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelsSliding1 = new ui.JPanelsSliding();
        questionPanel1 = new ui.QuestionPanel();
        questionPanel2 = new ui.QuestionPanel();
        lblHeading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Scholars Katta's NEET+JEE Software 2014");

        jPanel1.setBackground(new java.awt.Color(240, 248, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        jPanel3.setBackground(new java.awt.Color(240, 248, 255));
        jPanel3.setName("jPanel3"); // NOI18N

        btnNext.setBackground(new java.awt.Color(255, 255, 255));
        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnNext.setName("btnNext"); // NOI18N
        btnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNextMouseExited(evt);
            }
        });
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        txtQuestionNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtQuestionNumber.setText("0000");
        txtQuestionNumber.setName("txtQuestionNumber"); // NOI18N
        txtQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseExited(evt);
            }
        });
        txtQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionNumberKeyReleased(evt);
            }
        });

        cmbSub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Maths" }));
        cmbSub.setName("cmbSub"); // NOI18N
        cmbSub.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbSubMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cmbSubMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cmbSubMouseExited(evt);
            }
        });
        cmbSub.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSubItemStateChanged(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        btnFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        btnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnFirst.setName("btnFirst"); // NOI18N
        btnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnFirstMouseExited(evt);
            }
        });
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });

        btnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        btnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrevious.setIconTextGap(0);
        btnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPrevious.setName("btnPrevious"); // NOI18N
        btnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPreviousMouseExited(evt);
            }
        });
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });

        lblMsg.setText("Welcome To Practice Test Wizard.");
        lblMsg.setName("lblMsg"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnFirst)
                .addGap(5, 5, 5)
                .addComponent(btnPrevious)
                .addGap(5, 5, 5)
                .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(btnNext)
                .addGap(5, 5, 5)
                .addComponent(btnLast)
                .addGap(5, 5, 5)
                .addComponent(cmbSub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(156, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFirst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPrevious, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnNext, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnLast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(cmbSub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel1.add(jPanel3);

        jPanel2.setBackground(new java.awt.Color(240, 255, 255));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Animation :");
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel2.add(jLabel1);

        rdoEnable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoEnable.setSelected(true);
        rdoEnable.setText("Enable");
        rdoEnable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoEnable.setName("rdoEnable"); // NOI18N
        rdoEnable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoEnableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoEnable);

        rdoDisable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoDisable.setText("Disable");
        rdoDisable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoDisable.setName("rdoDisable"); // NOI18N
        rdoDisable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoDisableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoDisable);

        btnSuspend.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSuspend.setText("Suspend");
        btnSuspend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSuspend.setName("btnSuspend"); // NOI18N
        btnSuspend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSuspendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSuspendMouseExited(evt);
            }
        });
        btnSuspend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuspendActionPerformed(evt);
            }
        });
        jPanel2.add(btnSuspend);

        btnPause.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPause.setText("Pause");
        btnPause.setToolTipText("");
        btnPause.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPause.setName("btnPause"); // NOI18N
        btnPause.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPauseMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPauseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPauseMouseExited(evt);
            }
        });
        btnPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPauseActionPerformed(evt);
            }
        });
        jPanel2.add(btnPause);

        lblTimer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblTimer.setText("jLabel1");
        lblTimer.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        lblTimer.setName("lblTimer"); // NOI18N
        lblTimer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblTimerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblTimerMouseExited(evt);
            }
        });
        jPanel2.add(lblTimer);

        btnResume.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnResume.setText("Resume");
        btnResume.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnResume.setEnabled(false);
        btnResume.setName("btnResume"); // NOI18N
        btnResume.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnResumeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnResumeMouseExited(evt);
            }
        });
        btnResume.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResumeActionPerformed(evt);
            }
        });
        jPanel2.add(btnResume);

        btnSubmit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSubmit.setText("Submit & Next");
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSubmit.setName("btnSubmit"); // NOI18N
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSubmitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSubmitMouseExited(evt);
            }
        });
        btnSubmit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnSubmitItemStateChanged(evt);
            }
        });
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });
        btnSubmit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSubmitKeyPressed(evt);
            }
        });
        jPanel2.add(btnSubmit);

        btnEndPractice.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEndPractice.setText("End Practice");
        btnEndPractice.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEndPractice.setName("btnEndPractice"); // NOI18N
        btnEndPractice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEndPracticeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEndPracticeMouseExited(evt);
            }
        });
        btnEndPractice.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnEndPracticeItemStateChanged(evt);
            }
        });
        btnEndPractice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEndPracticeActionPerformed(evt);
            }
        });
        jPanel2.add(btnEndPractice);

        btnHint.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnHint.setForeground(new java.awt.Color(255, 0, 0));
        btnHint.setText("Hint");
        btnHint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHint.setName("btnHint"); // NOI18N
        btnHint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnHintMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHintMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHintMouseExited(evt);
            }
        });
        btnHint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHintActionPerformed(evt);
            }
        });
        jPanel2.add(btnHint);

        pnlAllQue.setBackground(new java.awt.Color(255, 255, 255));
        pnlAllQue.setName("pnlAllQue"); // NOI18N

        javax.swing.GroupLayout pnlAllQueLayout = new javax.swing.GroupLayout(pnlAllQue);
        pnlAllQue.setLayout(pnlAllQueLayout);
        pnlAllQueLayout.setHorizontalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 171, Short.MAX_VALUE)
        );
        pnlAllQueLayout.setVerticalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 428, Short.MAX_VALUE)
        );

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanelsSliding1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelsSliding1.setName("jPanelsSliding1"); // NOI18N
        jPanelsSliding1.setLayout(new java.awt.CardLayout());

        questionPanel1.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel1.setFont(new java.awt.Font("Centaur", 0, 11)); // NOI18N
        questionPanel1.setName("questionPanel1"); // NOI18N
        jPanelsSliding1.add(questionPanel1, "card2");

        questionPanel2.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel2.setName("questionPanel2"); // NOI18N
        jPanelsSliding1.add(questionPanel2, "card3");

        jScrollPane1.setViewportView(jPanelsSliding1);

        lblHeading.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblHeading.setText("Practice - -> Physics - -> Sets");
        lblHeading.setName("lblHeading"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 933, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 933, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 756, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlAllQue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblHeading, javax.swing.GroupLayout.DEFAULT_SIZE, 933, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlAllQue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblHeading)
                    .addContainerGap(521, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
    if(currentIndex!=0)
    {
//        setred();
        currentIndex=0;
        setPanel(true);
    }
    else
    JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnFirstActionPerformed
private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
    if(currentIndex!=0)
    {
//        setred();
        currentIndex--;
        setPanel(true);
    }
    else
        JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnPreviousActionPerformed
private void txtQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionNumberKeyReleased
if(evt.getKeyCode()==10)
        {
            int i = Integer.parseInt(txtQuestionNumber.getText());
            int size = alQuestions.size();
            if((i<1)||(i>size))
            {
                JOptionPane.showMessageDialog(rootPane, "Out Of Range Index");
            }                                     
            else
            {
//                setred();
                currentIndex=i-1;
                setPanel(false);
            }
        }
}//GEN-LAST:event_txtQuestionNumberKeyReleased

private void btnNextActionPerformed(java.awt.event.ActionEvent evt,boolean b) {                                        
    int last=alQuestions.size()-1;
    if(currentIndex==last)
    {
//        setred();
        currentIndex=0;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}      

private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
    int last=alQuestions.size()-1;
    if(currentIndex<last)
    {
//        setred();
        currentIndex++;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnNextActionPerformed
private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    int last=alQuestions.size()-1;
    if(currentIndex<last)
    {
//        setred();
        currentIndex=last;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnLastActionPerformed
private void btnSubmitItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnSubmitItemStateChanged
    
}//GEN-LAST:event_btnSubmitItemStateChanged
private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
    int last=alQuestions.size()-1;
    String userAnswer=currentPanel.getUserAnswer();
    int i=currentIndex;
    if(userAnswer==null)
    {
        JOptionPane.showMessageDialog(rootPane,"Please Select Option");
    }
    else
    {
        if(i<last){
            db.setQuestionStatus(alQuestions.get(currentIndex), practiceBean.getPracticeId(), userAnswer);
            jButtonsArray[i].setBackground(Color.green);
            jButtonsArray[i].setToolTipText("Answered");
            btnNextActionPerformed(evt);
        }
        else if(i==last)
        {
            db.setQuestionStatus(alQuestions.get(currentIndex), practiceBean.getPracticeId(), userAnswer);
            jButtonsArray[i].setBackground(Color.green);
            jButtonsArray[i].setToolTipText("Answered");
            JOptionPane.showMessageDialog(null,"Last Question is Submitted.");
            boolean b=true;
            btnNextActionPerformed(evt,b);
        }
    }
}//GEN-LAST:event_btnSubmitActionPerformed
private void btnEndPracticeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnEndPracticeItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_btnEndPracticeItemStateChanged
private void btnEndPracticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEndPracticeActionPerformed
    if(type==1){
        Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Submit Practice", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        timer.stop();
        new PracticeResultForm(practiceBean,rollNo).setVisible(true);
        this.dispose();
    }
    }
    else if(type==2){
        Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Submit Practice", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        new PracticeResultForm(uTestBean,rollNo).setVisible(true);
        this.dispose();
    }
    }
}//GEN-LAST:event_btnEndPracticeActionPerformed
private void btnPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPauseActionPerformed
    if(btnPause.getText().equals("Pause"))
    {
        pause();
        
        btnEndPractice.setEnabled(false);
        btnSubmit.setEnabled(false);
        txtQuestionNumber.setEnabled(false);
        btnPrevious.setEnabled(false);
        btnFirst.setEnabled(false);
        btnNext.setEnabled(false);
        btnLast.setEnabled(false); 
        btnPause.setEnabled(false);
        btnResume.setEnabled(true);
    }
    if(btnPause.getText().equals("Resume"))
        btnPause.setText("Pause");
}//GEN-LAST:event_btnPauseActionPerformed
private void btnResumeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResumeActionPerformed
    resume();
    btnEndPractice.setEnabled(true);
    btnSubmit.setEnabled(true);
    txtQuestionNumber.setEnabled(true);
    btnPrevious.setEnabled(true);
    btnFirst.setEnabled(true);
    btnNext.setEnabled(true);
    btnLast.setEnabled(true);
    btnPause.setEnabled(true);
    btnResume.setEnabled(false);
}//GEN-LAST:event_btnResumeActionPerformed
private void btnSuspendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuspendActionPerformed
    Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Suspend Test", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        practiceBean.setRemainingTime(remaining);
        practiceBean.setCurrentQuestionNumber(currentIndex);
        //SerializeTest.serialize(testBean); 
        SavePractice savePractice=new SavePractice();
        savePractice.savePracticeBean(practiceBean,rollNo);
        
        new NewTestForm(rollNo).setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_btnSuspendActionPerformed
private void rdoEnableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoEnableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }    
}//GEN-LAST:event_rdoEnableItemStateChanged
private void rdoDisableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoDisableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoDisableItemStateChanged
private void btnPauseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseClicked
    
}//GEN-LAST:event_btnPauseMouseClicked
private void btnSubmitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseEntered
    btnSubmit.setForeground(Color.red);
    lblMsg.setText("Submit Answer And View Next Question.");
}//GEN-LAST:event_btnSubmitMouseEntered
private void btnSubmitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseExited
    btnSubmit.setForeground(Color.black);
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnSubmitMouseExited
private void btnEndPracticeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndPracticeMouseEntered
    btnEndPractice.setForeground(Color.red);
    lblMsg.setText("End The Test And View Result.");
}//GEN-LAST:event_btnEndPracticeMouseEntered
private void btnEndPracticeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndPracticeMouseExited
    btnEndPractice.setForeground(Color.black);
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnEndPracticeMouseExited
private void btnSubmitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSubmitKeyPressed

}//GEN-LAST:event_btnSubmitKeyPressed
    private void btnHintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHintActionPerformed
        if(currentIndex<alQuestions.size())
        {
            QuestionBean questionBean = alQuestions.get(currentIndex);
            new HintForm1(questionBean, currentIndex+1).setVisible(true);
        }
    }//GEN-LAST:event_btnHintActionPerformed
private void btnHintMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseClicked

    // TODO add your handling code here:
}//GEN-LAST:event_btnHintMouseClicked
private void btnHintMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseEntered
    lblMsg.setText("Can View Hint Regarding Test.");
}//GEN-LAST:event_btnHintMouseEntered
private void btnHintMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseExited
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnHintMouseExited
private void btnPauseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseEntered
    btnPause.setForeground(Color.red);
    lblMsg.setText("Pause The Timer And Test.");
}//GEN-LAST:event_btnPauseMouseEntered
private void btnPauseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseExited
    btnPause.setForeground(Color.black);
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnPauseMouseExited
private void btnSuspendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSuspendMouseEntered
    btnSuspend.setForeground(Color.red);
    lblMsg.setText("Suspend Test And Can Resume Later.");
}//GEN-LAST:event_btnSuspendMouseEntered
private void btnSuspendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSuspendMouseExited
    btnSuspend.setForeground(Color.black);
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnSuspendMouseExited
private void btnResumeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResumeMouseEntered
    btnResume.setForeground(Color.red);
    lblMsg.setText("Resume Paused Test.");
}//GEN-LAST:event_btnResumeMouseEntered
private void btnResumeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResumeMouseExited
    btnResume.setForeground(Color.black);
    lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnResumeMouseExited

private void btnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseEntered
lblMsg.setText("Switch And View To First Question.");
}//GEN-LAST:event_btnFirstMouseEntered

private void btnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnFirstMouseExited

private void btnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseEntered
lblMsg.setText("Switch And View To Previous Question.");
}//GEN-LAST:event_btnPreviousMouseEntered

private void btnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnPreviousMouseExited

private void btnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseEntered
lblMsg.setText("Switch And View To Next Question.");
}//GEN-LAST:event_btnNextMouseEntered

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
lblMsg.setText("Switch And View To Last Question.");
}//GEN-LAST:event_btnLastMouseEntered

private void btnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnNextMouseExited

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_btnLastMouseExited

private void txtQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseEntered
lblMsg.setText("Enter Question No. Press Enter To View.");
}//GEN-LAST:event_txtQuestionNumberMouseEntered

private void txtQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_txtQuestionNumberMouseExited

private void cmbSubMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubMouseEntered
lblMsg.setText("Subject Of Witch Test Is Going On.");
}//GEN-LAST:event_cmbSubMouseEntered

private void cmbSubMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_cmbSubMouseExited

private void lblTimerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseEntered
lblMsg.setText("Show Time Of Test.");
}//GEN-LAST:event_lblTimerMouseEntered

private void lblTimerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseExited
lblMsg.setText("Welcome To Practice Test Wizard.");
}//GEN-LAST:event_lblTimerMouseExited

private void cmbSubMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubMouseClicked
    
}//GEN-LAST:event_cmbSubMouseClicked

private void cmbSubItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSubItemStateChanged

}//GEN-LAST:event_cmbSubItemStateChanged
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(StartedPracticeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(StartedPracticeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(StartedPracticeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(StartedPracticeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new StartedPracticeForm(7).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEndPractice;
    private javax.swing.JButton btnFirst;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JButton btnHint;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPause;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JButton btnResume;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JButton btnSuspend;
    private javax.swing.JComboBox cmbSub;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private ui.JPanelsSliding jPanelsSliding1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JLabel lblMsg;
    private javax.swing.JLabel lblTimer;
    private javax.swing.JPanel pnlAllQue;
    private ui.QuestionPanel questionPanel1;
    private ui.QuestionPanel questionPanel2;
    private javax.swing.JRadioButton rdoDisable;
    private javax.swing.JRadioButton rdoEnable;
    private javax.swing.JTextField txtQuestionNumber;
    // End of variables declaration//GEN-END:variables
}