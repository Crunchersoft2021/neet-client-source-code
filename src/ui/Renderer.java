/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import javax.swing.JLabel; 
import java.awt.Component; 
import javax.swing.JTable; 
import javax.swing.table.DefaultTableCellRenderer; 
  
public class Renderer extends DefaultTableCellRenderer{ 
  
    public void fillColor(JTable t,JLabel l,boolean isSelected ){ 
        //setting the background and foreground when JLabel is selected 
        if(isSelected){ 
            l.setBackground(t.getSelectionBackground()); 
            l.setForeground(t.getSelectionForeground()); 
        } 
  
        else{ 
            l.setBackground(t.getBackground()); 
            l.setForeground(t.getForeground()); 
        } 
  
    } 
  
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, 
         boolean hasFocus, int row, int column) 
     { 
  
        //In the method of getTableCellRendererComponent 
  
        if(value instanceof JLabel){ 
            JLabel label = (JLabel)value; 
            //you can add the image here 
            //label.setIcon(new ImageIcon("g://home.png"));
            //label.setOpaque(true); 
            fillColor(table,label,isSelected); 
  
            return label; 
        } 
  
//other stuff 
  
        else
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
     } 
  
}