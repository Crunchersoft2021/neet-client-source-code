/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import registration.CheckRegistration;
import registration.RegistrationForm;

public class StartFrame {

    public StartFrame() {
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {

            public void run() {
                ImageFrame frame = new ImageFrame();
                frame.setUndecorated(true);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                Timer timer = new Timer();
                boolean b = true;
                Tasker sound = new Tasker(frame);
                timer.schedule(sound, 4500);
            }
//            public  void check() {
//            
//       }
        });
    }
}

class ImageFrame extends JFrame {

    public ImageFrame() {
        setIconImage(new ImageIcon(getClass().getResource("/ui/images/c.gif")).getImage());
        File imagePath = new File("/ui/images/logo.gif");
        try {
            JLabel jLabel = new JLabel();
            Image image = ImageIO.read(imagePath);
            setSize(image.getWidth(rootPane), image.getHeight(rootPane));
            jLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/logo.gif")));
            this.add(jLabel);
            setLocationRelativeTo(null);
        } catch (IOException ex) {
            Logger.getLogger(ImageFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static final int DEFAULT_WIDTH = 300;
    public static final int DEFAULT_HEIGHT = 200;
}

class ImageComponent extends JComponent {

    private static final long serialVersionUID = 1L;
    private Image image;

    public ImageComponent(Image image) {
        this.image = image;
    }

    public void paintComponent(Graphics g) {
        if (image == null) {
            return;
        }
        int imageWidth = image.getWidth(this);
        int imageHeight = image.getHeight(this);

        g.drawImage(image, 0, 0, this);

        for (int i = 0; i * imageWidth <= getWidth(); i++) {
            for (int j = 0; j * imageHeight <= getHeight(); j++) {
                if (i + j > 0) {
                    g.copyArea(0, 0, imageWidth, imageHeight, i * imageWidth, j * imageHeight);
                }
            }
        }
    }
}

class Tasker extends TimerTask {

    JFrame frame1, frame2;

    public Tasker(JFrame frame1) {
        this.frame1 = frame1;
    }

    public void run() {
        frame1.dispose();
        CheckRegistration cs = new CheckRegistration();
        cs.checkRegistration();
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StartFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StartFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StartFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StartFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
}
