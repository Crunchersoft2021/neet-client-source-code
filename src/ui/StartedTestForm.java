package ui;
import JEE.test.SaveTest;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import JEE.practice.PracticeBean;

import JEE.test.DBConnection;
import JEE.test.TestBean;
import JEE.unitTest.UnitTestBean;
import com.bean.QuestionBean;

public class StartedTestForm extends javax.swing.JFrame {
    DBConnection db1=new DBConnection();
    ArrayList<Integer> E=new ArrayList<Integer>();
    QuestionPanel currentPanel=null;
    ArrayList<QuestionBean> alPhysics,alChemistry,alMaths;
    ArrayList<QuestionBean> alCurrent;
    boolean flagSubject=false,isNewTest=true;
    int currentIndex,quecount[];
    DBConnection db;
    TestBean testBean;
    PracticeBean praBean;
    UnitTestBean uTestBean;
    int animationTime=5;
    private javax.swing.JButton[] jButtonsArrayPhysics,jButtonsArrayMaths,jButtonsArrayChemistry;
    Image bg=new ImageIcon("src/ui/images/wallpaper-1.jpg").getImage();
    JPanel[] queButtonPanels ;
    long remaining; // How many milliseconds remain in the countdown.
    long lastUpdate; // When count was last updated  
    Timer timer; // Updates the count every second
    NumberFormat format;
     int rollNo;
    /** Creates new form TestResultForm */
    public StartedTestForm(int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setState(JFrame.MAXIMIZED_BOTH);
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        cl.show(jPanelsSliding1, "card4");
        currentPanel=questionPanel1;
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        db=new DBConnection();
        //setallque();
        btnSubmit.addKeyListener(new MyKeyListener());
    }

    public void removeallque()
    {
        pnlAllQue.removeAll();
    }
    
    public void setButtonOnPanel(JPanel panel,JButton[] buttonArray, int count,int subject)
    {        
        for(int x = 0; x < count ; x++){
                buttonArray[x] = new javax.swing.JButton();
                QuestionBean q = new QuestionBean();
                if(subject==0){
                    q=alPhysics.get(x);
                }
                else if(subject==1){
                    q=alChemistry.get(x);
                }
                else if(subject==2){
                    q=alMaths.get(x);
                }
                if(q.getUserAnswer().equals("UnAttempted")){
                    buttonArray[x].setBackground(Color.white);
                }
                else{
                    buttonArray[x].setBackground(Color.green);
                }
                buttonArray[x].setToolTipText("Not Answered");
                buttonArray[x].setActionCommand(subject+" "+x);
                buttonArray[x].addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                int y=x+1;
                buttonArray[x].setText(""+y);
       }
       GridBagConstraints cons=new GridBagConstraints();
            cons.gridx = 0;   cons.gridy  =  0;
            cons.gridwidth = 1;   cons.gridheight =  1;
            cons.anchor  =  GridBagConstraints.BELOW_BASELINE_LEADING;
            cons.weightx =  2;   cons.weighty =  1;
            cons.insets=new java.awt.Insets(1, 1, 1, 1);
            GridBagLayout layout = new GridBagLayout();
            for(int x=0;x<count;x++)
            {
                if(x%18==0)
                {
                    cons.gridx++;
                    cons.gridy=1;
                }                
                layout.setConstraints(buttonArray[x], cons);
                panel.setLayout(layout);
                panel.add(buttonArray[x],cons);                
                cons.gridy++;
            }
    }
    
    public void createPanelObjects()
    {
        int count=0;
        count=alPhysics.size();                
        if(count>0)
        {
            queButtonPanels[0]=new JPanel();
            queButtonPanels[0].setBackground(Color.white);
            jButtonsArrayPhysics=new JButton[count];
            setButtonOnPanel(queButtonPanels[0], jButtonsArrayPhysics, count, 0);            
        }
        else{
            queButtonPanels[0]=null;
        }
        count=alChemistry.size();                
        if(count>0)
        {
            queButtonPanels[1]=new JPanel();
            queButtonPanels[1].setBackground(Color.white);
            jButtonsArrayChemistry=new JButton[count];
            setButtonOnPanel(queButtonPanels[1], jButtonsArrayChemistry, count, 1);            
        }
        else{
            queButtonPanels[1]=null;
        }
        count=alMaths.size();                
        if(count>0)
        {
            queButtonPanels[2]=new JPanel();
            queButtonPanels[2].setBackground(Color.white);
            jButtonsArrayMaths=new JButton[count];
            setButtonOnPanel(queButtonPanels[2], jButtonsArrayMaths, count, 2);            
        }
        else{
            queButtonPanels[2]=null;
        }
    }

    public void createPanelObjects(int subid)
    {
        int count=0;
        if(subid==1){
        count=alPhysics.size();                
        if(count>0)
        {
            queButtonPanels[0]=new JPanel();
            queButtonPanels[0].setBackground(Color.white);
            jButtonsArrayPhysics=new JButton[count];
            setButtonOnPanel(queButtonPanels[0], jButtonsArrayPhysics, count, 0);            
        }
        else{
            queButtonPanels[0]=null;
        }}
        else if(subid==2){
        count=alChemistry.size();                
        if(count>0)
        {
            queButtonPanels[1]=new JPanel();
            queButtonPanels[1].setBackground(Color.white);
            jButtonsArrayChemistry=new JButton[count];
            setButtonOnPanel(queButtonPanels[1], jButtonsArrayChemistry, count, 1);            
        }
        else{
            queButtonPanels[1]=null;
        }}else if(subid==3){
        count=alMaths.size();                
        if(count>0)
        {
            queButtonPanels[2]=new JPanel();
            queButtonPanels[2].setBackground(Color.white);
            jButtonsArrayMaths=new JButton[count];
            setButtonOnPanel(queButtonPanels[2], jButtonsArrayMaths, count, 2);            
        }
        else{
            queButtonPanels[2]=null;
        }}
    }

    
    public void setPanel(boolean flag)
    {
        currentPanel=(currentPanel==questionPanel1)?questionPanel2:questionPanel1;
        currentPanel.unlockSelection();    
        //set Question        
        currentPanel.setQuestionOnPanel(alCurrent.get(currentIndex),(currentIndex+1));
        txtQuestionNumber.setText((currentIndex+1)+"");
        //slide panel    
        jPanelsSliding1.nextSlidPanel(animationTime,currentPanel,flag);
        System.out.println("Time : "+animationTime);
        jPanelsSliding1.refresh();
        //removeallque();
        //setallque();
    }
    public void setQuestion(String actionCommand)
    {
//        setred();
        currentIndex=Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }
    
    public StartedTestForm(TestBean testBean,boolean newTest,int rollNo) {
        initComponents();  
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        db=new DBConnection();
        this.testBean=testBean;
        currentPanel=questionPanel1;
        ArrayList<Integer> E=new ArrayList<Integer>();
        this.alPhysics = testBean.getAlPhysics();
        this.alChemistry = testBean.getAlChemistry();
        this.alMaths = testBean.getAlMaths();
        queButtonPanels = new JPanel[4];
        //cl.show(jPanelsSliding1, "card4");
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        
        switch(testBean.getGroupId())
        {
            case 1: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    lblHeading.setText("Test - - ->Physics.");
                    break;
            case 2: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    lblHeading.setText("Test - - ->Chemistry.");
                    break;
            case 3: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    lblHeading.setText("Test - - ->Maths.");
                    break;
            case 4: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Maths" }));
                    lblHeading.setText("Test - - ->PCM.");
                    break;                
        }

        cmbSubject.setSelectedIndex(-1);   
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        createPanelObjects();
        if(newTest)
        {
            db.saveStateOfNewTest(testBean,rollNo);
            
            int minutes= testBean.getTotalTime();
            //System.out.println(minutes+" min");
            if (minutes > 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            cmbSubject.setSelectedIndex(0);
        }
        else
        {
            
            remaining = testBean.getRemainingTime();  
            isNewTest = false;
            cmbSubject.setSelectedIndex(testBean.getCurrentSubjectNumber());
        }
        timer.setInitialDelay(0); 
        timer.start();
    }
   
  public class MyKeyListener extends KeyAdapter{
  public void keyPressed(KeyEvent ke){
  char i = ke.getKeyChar();}
  }  
    
  public void start() {
  resume();
  } // Start displaying updates
  public void stop() {
    pause();
  }
  void resume() {
    // Restore the time we're counting down from and restart the timer.
    lastUpdate = System.currentTimeMillis();
    timer.start(); // Start the timer
  }
  void pause() {
    long now = System.currentTimeMillis();
    remaining -= (now - lastUpdate);
    timer.stop(); // Stop the timer
  }
  
  void updateDisplay() {
    long now = System.currentTimeMillis(); // current time in ms
    long elapsed = now - lastUpdate; // ms elapsed since last update
    remaining -= elapsed; // adjust remaining time
    lastUpdate = now; // remember this update time

    // Convert remaining milliseconds to mm:ss format and display
    if (remaining < 0)
      remaining = 0;
    int hours   =  (int)(remaining / 3600000);
    int minutes = (int) ((remaining % 3600000) / 60000);
    int seconds = (int) ((remaining % 60000) / 1000);
    lblTimer.setText(format.format(hours) + ":" +format.format(minutes) + ":" + format.format(seconds));

    // If we've completed the countdown beep and display new page
    if (remaining == 0) {
      // Stop updating now.
      timer.stop();
      
      JOptionPane.showMessageDialog(this,"Test Finished"); 
      this.dispose();
      new TestResultForm(testBean,rollNo).setVisible(true);
      }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        cmbSubject = new javax.swing.JComboBox();
        btnLast = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        txtQuestionNumber = new javax.swing.JTextField();
        btnPrevious = new javax.swing.JButton();
        btnFirst = new javax.swing.JButton();
        lblMsg = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rdoEnable = new javax.swing.JRadioButton();
        rdoDisable = new javax.swing.JRadioButton();
        btnSuspend = new javax.swing.JButton();
        btnPause = new javax.swing.JButton();
        lblTimer = new javax.swing.JLabel();
        btnResume = new javax.swing.JButton();
        btnSubmit = new javax.swing.JButton();
        btnEndExam = new javax.swing.JButton();
        pnlAllQue = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelsSliding1 = new ui.JPanelsSliding();
        questionPanel1 = new ui.QuestionPanel();
        questionPanel2 = new ui.QuestionPanel();
        lblHeading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Scholars Katta's NEET+JEE Software 2014");

        jPanel1.setBackground(new java.awt.Color(240, 248, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        jPanel3.setBackground(new java.awt.Color(240, 248, 255));
        jPanel3.setName("jPanel3"); // NOI18N

        cmbSubject.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Maths" }));
        cmbSubject.setName("cmbSubject"); // NOI18N
        cmbSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cmbSubjectMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cmbSubjectMouseExited(evt);
            }
        });
        cmbSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSubjectItemStateChanged(evt);
            }
        });
        cmbSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectActionPerformed(evt);
            }
        });

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        btnNext.setBackground(new java.awt.Color(255, 255, 255));
        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnNext.setName("btnNext"); // NOI18N
        btnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNextMouseExited(evt);
            }
        });
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        txtQuestionNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtQuestionNumber.setText("0000");
        txtQuestionNumber.setName("txtQuestionNumber"); // NOI18N
        txtQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseExited(evt);
            }
        });
        txtQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionNumberKeyReleased(evt);
            }
        });

        btnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        btnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrevious.setIconTextGap(0);
        btnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPrevious.setName("btnPrevious"); // NOI18N
        btnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPreviousMouseExited(evt);
            }
        });
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });

        btnFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        btnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnFirst.setName("btnFirst"); // NOI18N
        btnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnFirstMouseExited(evt);
            }
        });
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });

        lblMsg.setText("Welcome To Test Wizard.");
        lblMsg.setName("lblMsg"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFirst)
                .addGap(5, 5, 5)
                .addComponent(btnPrevious)
                .addGap(5, 5, 5)
                .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(btnNext)
                .addGap(5, 5, 5)
                .addComponent(btnLast)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(237, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(btnFirst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrevious, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(btnNext, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3);

        jPanel2.setBackground(new java.awt.Color(240, 255, 255));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Animation :");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel2.add(jLabel1);

        rdoEnable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoEnable.setSelected(true);
        rdoEnable.setText("Enable");
        rdoEnable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoEnable.setName("rdoEnable"); // NOI18N
        rdoEnable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoEnableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoEnable);

        rdoDisable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoDisable.setText("Disable");
        rdoDisable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoDisable.setName("rdoDisable"); // NOI18N
        rdoDisable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoDisableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoDisable);

        btnSuspend.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSuspend.setText("Suspend");
        btnSuspend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSuspend.setName("btnSuspend"); // NOI18N
        btnSuspend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSuspendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSuspendMouseExited(evt);
            }
        });
        btnSuspend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuspendActionPerformed(evt);
            }
        });
        jPanel2.add(btnSuspend);

        btnPause.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPause.setText("Pause");
        btnPause.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPause.setName("btnPause"); // NOI18N
        btnPause.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPauseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPauseMouseExited(evt);
            }
        });
        btnPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPauseActionPerformed(evt);
            }
        });
        jPanel2.add(btnPause);

        lblTimer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblTimer.setText("jLabel1");
        lblTimer.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        lblTimer.setName("lblTimer"); // NOI18N
        lblTimer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblTimerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblTimerMouseExited(evt);
            }
        });
        jPanel2.add(lblTimer);

        btnResume.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnResume.setText("Resume");
        btnResume.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnResume.setEnabled(false);
        btnResume.setName("btnResume"); // NOI18N
        btnResume.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnResumeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnResumeMouseExited(evt);
            }
        });
        btnResume.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResumeActionPerformed(evt);
            }
        });
        jPanel2.add(btnResume);

        btnSubmit.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSubmit.setText("Submit & Next");
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSubmit.setName("btnSubmit"); // NOI18N
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSubmitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSubmitMouseExited(evt);
            }
        });
        btnSubmit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnSubmitItemStateChanged(evt);
            }
        });
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });
        btnSubmit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSubmitKeyPressed(evt);
            }
        });
        jPanel2.add(btnSubmit);

        btnEndExam.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEndExam.setText("End Exam");
        btnEndExam.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEndExam.setName("btnEndExam"); // NOI18N
        btnEndExam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEndExamMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEndExamMouseExited(evt);
            }
        });
        btnEndExam.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnEndExamItemStateChanged(evt);
            }
        });
        btnEndExam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEndExamActionPerformed(evt);
            }
        });
        jPanel2.add(btnEndExam);

        pnlAllQue.setBackground(new java.awt.Color(255, 255, 255));
        pnlAllQue.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlAllQue.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pnlAllQue.setName("pnlAllQue"); // NOI18N

        javax.swing.GroupLayout pnlAllQueLayout = new javax.swing.GroupLayout(pnlAllQue);
        pnlAllQue.setLayout(pnlAllQueLayout);
        pnlAllQueLayout.setHorizontalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 179, Short.MAX_VALUE)
        );
        pnlAllQueLayout.setVerticalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 376, Short.MAX_VALUE)
        );

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanelsSliding1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelsSliding1.setName("jPanelsSliding1"); // NOI18N
        jPanelsSliding1.setLayout(new java.awt.CardLayout());

        questionPanel1.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel1.setName("questionPanel1"); // NOI18N
        jPanelsSliding1.add(questionPanel1, "card2");

        questionPanel2.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel2.setName("questionPanel2"); // NOI18N
        jPanelsSliding1.add(questionPanel2, "card3");

        jScrollPane1.setViewportView(jPanelsSliding1);

        lblHeading.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblHeading.setText("Practice - -> Physics - -> Sets");
        lblHeading.setName("lblHeading"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlAllQue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE))
                        .addContainerGap())))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblHeading, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlAllQue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(lblHeading)
                    .addContainerGap(477, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
    if(currentIndex!=0)
    {
//        setred();
       currentIndex=0;
        setPanel(true);   
    }
    else
        JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnFirstActionPerformed

private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
    if(currentIndex!=0)
    {
//        setred();
        currentIndex--;
        setPanel(true);
    }
    else
        JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnPreviousActionPerformed

private void txtQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionNumberKeyReleased
if(evt.getKeyCode()==10)
{
    int i = Integer.parseInt(txtQuestionNumber.getText());
    int size = alCurrent.size();
    if((i<1)||(i>size))
    {
        JOptionPane.showMessageDialog(rootPane, "Out Of Range Index");
    }                                     
    else
    {
//        setred();
        currentIndex=i-1;
        setPanel(false);
    }
    }
}//GEN-LAST:event_txtQuestionNumberKeyReleased

public void paintComponent(Graphics g) {
        
        g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
    }

private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
    int last=alCurrent.size()-1;
    if(currentIndex==last){
        currentIndex=0;
        setPanel(false);
    }
    else if(currentIndex<last)
    {
//        setred();
        currentIndex++;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnNextActionPerformed

private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    int last=alCurrent.size()-1;
    if(currentIndex<last)
    {
//        setred();
        currentIndex=last;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnLastActionPerformed

private void cmbSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSubjectItemStateChanged
    ui.QuestionPanel setimage=new QuestionPanel();
    pnlAllQue.repaint();
    if(flagSubject)
    {        
        int subjectId;
        QuestionPanel qp=new QuestionPanel();
        String subject = cmbSubject.getSelectedItem().toString();
        if(subject.equals("Physics"))
        {
            pnlAllQue.removeAll();
            alCurrent = alPhysics;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[0]);
        }
        else if(subject.equals("Chemistry"))
        {
            pnlAllQue.removeAll();
            alCurrent = alChemistry;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[1]);
        }
        else if(subject.equals("Maths"))
        {
            pnlAllQue.removeAll();
            alCurrent = alMaths;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[2]);
        }
        else
        {
            alCurrent = null;
            pnlAllQue.removeAll();
        }
        
        if(alCurrent!=null)
        {
            if(isNewTest)
            {
                currentIndex=0;
            }
            else
            {
                currentIndex = testBean.getCurrentQuestionNumber();
            }                        
            setPanel(false);            
            
        }
        flagSubject=false;
    }
    else
    {
        flagSubject=true;
    }
}//GEN-LAST:event_cmbSubjectItemStateChanged

private void btnSubmitItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnSubmitItemStateChanged
    
}//GEN-LAST:event_btnSubmitItemStateChanged

private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
    String userAnswer=currentPanel.getUserAnswer();
    int i=currentIndex;
    if(userAnswer==null)
    {
        JOptionPane.showMessageDialog(rootPane,"Please Select Option");
    }
    else
    {
        if(i<=50||i<=100){
            db.setQuestionStatus(alCurrent.get(currentIndex), testBean.getTestId(), userAnswer);
            if(cmbSubject.getSelectedItem().equals("Physics")){
                jButtonsArrayPhysics[i].setBackground(Color.green);
                jButtonsArrayPhysics[i].setToolTipText("Answered");
            }
            if(cmbSubject.getSelectedItem().equals("Chemistry")){
                jButtonsArrayChemistry[i].setBackground(Color.green);
                jButtonsArrayChemistry[i].setToolTipText("Answered");
            }
            if(cmbSubject.getSelectedItem().equals("Maths")){
                jButtonsArrayMaths[i].setBackground(Color.green);
                jButtonsArrayMaths[i].setToolTipText("Answered");
            }
            btnNextActionPerformed(evt);
        }
        else{
            Object[] options = { "Exit" };
            int j = JOptionPane.showOptionDialog(null, "Your Result is...", "Result",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null, options, options[0]);
            if(i==0)
            {
                new TestResultForm(testBean,rollNo).setVisible(true);
                this.dispose();
            }
        }
    }
}//GEN-LAST:event_btnSubmitActionPerformed

private void btnEndExamItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnEndExamItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_btnEndExamItemStateChanged

private void btnEndExamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEndExamActionPerformed
    Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Submit Test", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        timer.stop();
        new TestResultForm(testBean,rollNo).setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_btnEndExamActionPerformed

private void btnPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPauseActionPerformed
    pause();
    btnSubmit.setEnabled(false);
    btnEndExam.setEnabled(false);
    btnFirst.setEnabled(false);
    btnPrevious.setEnabled(false);
    btnNext.setEnabled(false);
    btnLast.setEnabled(false);
    txtQuestionNumber.setEnabled(false);
    btnResume.setEnabled(true);
    btnPause.setEnabled(false);
}//GEN-LAST:event_btnPauseActionPerformed

private void btnResumeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResumeActionPerformed
    resume();
    btnResume.setEnabled(false);
    btnPause.setEnabled(true);
    btnSubmit.setEnabled(true);
    btnEndExam.setEnabled(true);
    btnFirst.setEnabled(true);
    btnPrevious.setEnabled(true);
    btnNext.setEnabled(true);
    btnLast.setEnabled(true);
    txtQuestionNumber.setEnabled(true);
}//GEN-LAST:event_btnResumeActionPerformed

private void btnSuspendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuspendActionPerformed
    Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Suspend Test", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        testBean.setRemainingTime(remaining);
        testBean.setCurrentSubjectNumber(cmbSubject.getSelectedIndex());
        testBean.setCurrentQuestionNumber(currentIndex);
        //SerializeTest.serialize(testBean); 
        SaveTest saveTest=new SaveTest();
        saveTest.saveTestBean(testBean,rollNo);
        timer.stop();
        new NewTestForm(rollNo).setVisible(true);
        this.dispose();
    }
}//GEN-LAST:event_btnSuspendActionPerformed

private void rdoEnableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoEnableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }    
}//GEN-LAST:event_rdoEnableItemStateChanged

private void rdoDisableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoDisableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoDisableItemStateChanged

private void btnSubmitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseEntered
    btnSubmit.setForeground(Color.red);
    lblMsg.setText("Submit Answer And View Next Question.");
}//GEN-LAST:event_btnSubmitMouseEntered

private void btnSubmitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseExited
    btnSubmit.setForeground(Color.black);
    lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnSubmitMouseExited

private void btnEndExamMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndExamMouseEntered
    btnEndExam.setForeground(Color.red);
    lblMsg.setText("End The Test And View Result.");
}//GEN-LAST:event_btnEndExamMouseEntered

private void btnEndExamMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndExamMouseExited
    btnEndExam.setForeground(Color.black);
    lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnEndExamMouseExited

    private void btnSubmitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSubmitKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSubmitKeyPressed

    private void cmbSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSubjectActionPerformed

private void btnPauseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseEntered
    btnPause.setForeground(Color.red);
    lblMsg.setText("Pause The Timer And Test.");
}//GEN-LAST:event_btnPauseMouseEntered

private void btnPauseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseExited
    btnPause.setForeground(Color.black);
    lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnPauseMouseExited

private void btnSuspendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSuspendMouseEntered
    btnSuspend.setForeground(Color.red);
    lblMsg.setText("Suspend Test And Can Resume Later.");
}//GEN-LAST:event_btnSuspendMouseEntered

private void btnSuspendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSuspendMouseExited
    btnSuspend.setForeground(Color.black);
    lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnSuspendMouseExited

private void btnResumeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResumeMouseEntered
    btnResume.setForeground(Color.red);
    lblMsg.setText("Resume Paused Test.");
}//GEN-LAST:event_btnResumeMouseEntered

private void btnResumeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnResumeMouseExited
    btnResume.setForeground(Color.black);
    lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnResumeMouseExited

private void btnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnFirstMouseExited

private void btnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnPreviousMouseExited

private void txtQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_txtQuestionNumberMouseExited

private void btnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnNextMouseExited

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_btnLastMouseExited

private void cmbSubjectMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubjectMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_cmbSubjectMouseExited

private void lblTimerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseExited
lblMsg.setText("Welcome To Test Wizard.");
}//GEN-LAST:event_lblTimerMouseExited

private void lblTimerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseEntered
lblMsg.setText("Show Time Of Test.");
}//GEN-LAST:event_lblTimerMouseEntered

private void btnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseEntered
lblMsg.setText("Switch And View To First Question.");
}//GEN-LAST:event_btnFirstMouseEntered

private void btnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseEntered
lblMsg.setText("Switch And View To Previous Question.");
}//GEN-LAST:event_btnPreviousMouseEntered

private void btnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseEntered
lblMsg.setText("Switch And View To Next Question.");
}//GEN-LAST:event_btnNextMouseEntered

private void txtQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseEntered
lblMsg.setText("Enter Question No. Press Enter To View.");
}//GEN-LAST:event_txtQuestionNumberMouseEntered

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
lblMsg.setText("Switch And View To Last Question.");
}//GEN-LAST:event_btnLastMouseEntered

private void cmbSubjectMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbSubjectMouseEntered
lblMsg.setText("Subject Of Witch Test Is Going On.");
}//GEN-LAST:event_cmbSubjectMouseEntered
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new StartedTestForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEndExam;
    private javax.swing.JButton btnFirst;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPause;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JButton btnResume;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JButton btnSuspend;
    private javax.swing.JComboBox cmbSubject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private ui.JPanelsSliding jPanelsSliding1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JLabel lblMsg;
    private javax.swing.JLabel lblTimer;
    private javax.swing.JPanel pnlAllQue;
    private ui.QuestionPanel questionPanel1;
    private ui.QuestionPanel questionPanel2;
    private javax.swing.JRadioButton rdoDisable;
    private javax.swing.JRadioButton rdoEnable;
    private javax.swing.JTextField txtQuestionNumber;
    // End of variables declaration//GEN-END:variables
}