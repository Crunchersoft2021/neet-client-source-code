/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JEE.unitTest;

import com.bean.ClassTestBean;
import com.bean.QuestionBean;
import com.bean.RankBean;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.*;
import javax.swing.JOptionPane;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import server.DBConnection1;
import server.Server;

/**
 *
 * @author 007
 */
public class DBConnection {
//    private static String driver = "org.apache.derby.jdbc.EmbeddedDriver";
//    private static String protocol = "jdbc:derby:CETJeeNew;create=true;dataEncryption=true;bootPassword=*007_Cruncher+Soft@Wagholi_007*";
    //Connection con1,con2,conn;

    ArrayList<Integer> E = new ArrayList<Integer>();
//    Statement s,st1,st2;
//    ResultSet rs,rs1,rs2;
    
    private Connection con=null;
     private Statement s=null;
     private ResultSet rs=null,rs1=null;
     private PreparedStatement ps=null;
    int chaptercount;

    public DBConnection() {
    }

    public UnitTestBean getUnitTestBean(int subjectId, ArrayList<Integer> chapterId) {
        int questionCount = 30, totalTime = 20;
        try {
            ArrayList<QuestionBean> alQuestions = getQuestionsForUnitTest(subjectId, chapterId, questionCount);
            UnitTestBean unitTestBean = new UnitTestBean();
            unitTestBean.setSubjectId(subjectId);
            unitTestBean.setChapterId(chapterId);
            unitTestBean.setTotalTime(totalTime / 2);
            unitTestBean.setRemainingTime(totalTime);
            unitTestBean.setQuestions(alQuestions);
            return unitTestBean;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<ArrayList<QuestionBean>> getAllQuestionBean(int subjectId, ArrayList<Integer> chapterId) {
        ArrayList<ArrayList<QuestionBean>> allque = new ArrayList<ArrayList<QuestionBean>>();
        try {
            for (int i = 0; i < chapterId.size(); i++) {
                ArrayList<QuestionBean> alQuestions = getAllQuestions(subjectId, chapterId.get(i));
                allque.add(alQuestions);
            }
            return allque;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void delTest(int testId) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            String sql = "delete from Saved_Test_Info where id=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, testId);
            int i = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private ArrayList<QuestionBean> getQuestionsForUnitTest(int subjectId, ArrayList<Integer> chapterIds, int questionCount) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            QuestionBean questionBean=new QuestionBean();
            Statement s = con.createStatement();
            int chapterCount = chapterIds.size();
            int num = questionCount / chapterCount;
            int remaining = questionCount - (chapterCount * num);
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            for (int i = 0; i < chapterIds.size(); i++) {
                int questionPerChapter = num;
                if (i == 0) {
                    questionPerChapter += remaining;
                }
                rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + subjectId + " and Chapter_Id =" + chapterIds.get(i) + " order by Question_Id");
                ArrayList<QuestionBean> altemp = new ArrayList<QuestionBean>();
                while (rs1.next()) {
                questionBean.setQuestionId(rs1.getInt(1));
                questionBean.setQuestion(rs1.getString(2));
                questionBean.setOptionA(rs1.getString(3));
                questionBean.setOptionB(rs1.getString(4));
                questionBean.setOptionC(rs1.getString(5));
                questionBean.setOptionD(rs1.getString(6));
                questionBean.setAnswer(rs1.getString(7));
                questionBean.setHint(rs1.getString(8));
                questionBean.setLevel(rs1.getInt(9));
                questionBean.setSubjectId(rs1.getInt(10));
                questionBean.setChapterId(rs1.getInt(11));
                questionBean.setTopicId(rs1.getInt(12));
                questionBean.setQuestionImagePath(rs1.getString(14));
                questionBean.setOptionImagePath(rs1.getString(16));
                questionBean.setHintImagePath(rs1.getString(18));
                boolean result = (rs1.getInt(13) == 0) ? false : true;
                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                questionBean.setIsQuestionAsImage(result);
                questionBean.setIsOptionAsImage(result1);
                questionBean.setIsHintAsImage(result2);
                
                questionBean.setAttempt(rs1.getInt(19));
                questionBean.setType(rs1.getInt(20));
                int view = 0;
                questionBean.setYear(rs1.getString(21));
                
                al.add(questionBean);
                }
                Collections.shuffle(altemp);
                for (int j = 0; j < questionPerChapter; j++) {
                    al.add(altemp.get(j));
                }
            }
            return al;
        } catch (Exception e) {
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private ArrayList<QuestionBean> getAllQuestions(int subjectId, int chapterIds) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            QuestionBean questionBean=new QuestionBean();
            Statement s = con.createStatement();
            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + subjectId + " and Chapter_Id =" + chapterIds + " order by Question_Id");
            ArrayList<QuestionBean> altemp = new ArrayList<QuestionBean>();
            while (rs1.next()) {
                questionBean.setQuestionId(rs1.getInt(1));
                questionBean.setQuestion(rs1.getString(2));
                questionBean.setOptionA(rs1.getString(3));
                questionBean.setOptionB(rs1.getString(4));
                questionBean.setOptionC(rs1.getString(5));
                questionBean.setOptionD(rs1.getString(6));
                questionBean.setAnswer(rs1.getString(7));
                questionBean.setHint(rs1.getString(8));
                questionBean.setLevel(rs1.getInt(9));
                questionBean.setSubjectId(rs1.getInt(10));
                questionBean.setChapterId(rs1.getInt(11));
                questionBean.setTopicId(rs1.getInt(12));
                questionBean.setQuestionImagePath(rs1.getString(14));
                questionBean.setOptionImagePath(rs1.getString(16));
                questionBean.setHintImagePath(rs1.getString(18));
                boolean result = (rs1.getInt(13) == 0) ? false : true;
                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                questionBean.setIsQuestionAsImage(result);
                questionBean.setIsOptionAsImage(result1);
                questionBean.setIsHintAsImage(result2);
                
                questionBean.setAttempt(rs1.getInt(19));
                questionBean.setType(rs1.getInt(20));
                int view = 0;
                questionBean.setYear(rs1.getString(21));
                
                altemp.add(questionBean);
            }
            return altemp;
        } catch (Exception e) {
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public void saveStateOfNewPractice(UnitTestBean unitTestBean, int rollNo) {
        int uniITestId = 0;
        if (unitTestBean != null) {
            Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = con.createStatement();
                rs = s.executeQuery("Select max(unitTestId) from UTest_Info");
                if (rs.next()) {
                    uniITestId = rs.getInt(1);
                }
                uniITestId++;
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);
                PreparedStatement ps = con.prepareStatement("insert into UTest_Info values(?,?,?,?,?,?)");
                ps.setInt(1, uniITestId);
                ps.setInt(2, rollNo);
                ps.setInt(3, unitTestBean.getSubjectId());
                ps.setString(5, "Started");
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                ArrayList<Integer> chapterIds = unitTestBean.getChapterId();
                for (int i = 0; i < chapterIds.size(); i++) {
                    ps.setInt(4, chapterIds.get(i));
                    int executeUpdate = ps.executeUpdate();
                }
                unitTestBean.setUnitTestId(uniITestId);
                ArrayList<QuestionBean> alQuestions = unitTestBean.getQuestions();
                Iterator<QuestionBean> it = alQuestions.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    ps = con.prepareStatement("insert into UTest_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, uniITestId);
                    ps.setInt(2, question.getQuestionId());
                    ps.setString(3, "unAttempted");
                    ps.executeUpdate();
                }
                unitTestBean.setStatus("Started");
                unitTestBean.setStartDateTime(new java.util.Date());
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void setQuestionStatus(QuestionBean question, int unitTestId, String userAnswer,int Rollno) {
        if (question != null) {
            Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = con.createStatement();
                s.executeUpdate("update UTest_Question_Status_Info set user_Answer='" + userAnswer + "' where Question_Id=" + question.getQuestionId() + " and unitTestId=" + unitTestId + "and ROLLNO="+Rollno);
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public ArrayList<Integer> getChapIds(ArrayList<String> nameofchap) {
        ArrayList<Integer> noofchap = new ArrayList<Integer>();
        int i = nameofchap.size();
        int Chap_Id;
        for (int m = 0; m < i; m++) {
            Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs1;
            try {
                Statement s = con.createStatement();
                String name = nameofchap.get(m);

                rs1 = s.executeQuery("Select Chapter_Id from Chapter_Info where Chapter_Name ='" + name + "'");
                while (rs1.next()) {
                    Chap_Id = rs1.getInt(1);
                    noofchap.add(Chap_Id);

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return noofchap;
    }

    public double getPerwrongQuestionMarks(int subjectId) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public double getPerQuestionMarks(int subjectId) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select * from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(3);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public String getResult(int unitTestId, int rollNo,int Questionid) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select user_Answer from  UTest_Question_Status_Info where unitTestId=" + unitTestId + " and ROLLNO=" + rollNo + "and QUESTION_ID="+Questionid);
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    //Class_Test_Result_Info (test_Id INTEGER NOT NULL,rollno INTEGER NOT NULL,subject_Id INTEGER NOT NULL,total_Questions INTEGER NOT NULL,correct_Questions INTEGER NOT NULL,test_DateTime TIMESTAMP NOT NULL,PRIMARY KEY (test_Id,subject_Id))");

    public void addResult1(int test_Id, int rollno, int subject_Id, int total_Questions, int correct_Questions, int incorrect_Questions,double OBTAIN_MARK,double TotalMark,int Phy_correct_Questions,int Chem_correct_Questions,int Bio_correct_Questions,
            int Phy_incorrect_Questions,int Chem_incorrect_Questions,int Bio_incorrect_Questions,int TOTAL_PHY_QUESTIONS,int TOTAL_CHEM_QUESTIONS,int TOTAL_Bio_QUESTIONS,double TOTAL_PHY_MARK,double TOTAL_CHEM_MARK,double TOTAL_Bio_MARK,ClassTestBean classTestBean) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            int tid = 1;
            Statement ss = con.createStatement();
            ResultSet rrr = ss.executeQuery("select Max(TestBeanId) from StudentTestBean");
            if (rrr.next()) {
                tid = rrr.getInt(1);
            }
            Statement s1 = con.createStatement();
            s1.executeUpdate("update CurrentTestStatusOfStudent set statusoftest=1 where rollno=" + rollno);
            String sql = "insert into Class_Test_Result_Info values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, test_Id);
            ps.setInt(2, rollno);
            ps.setInt(3, subject_Id);
            ps.setInt(4, total_Questions);
            ps.setInt(5, correct_Questions);
            ps.setInt(6, incorrect_Questions);
            ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            ps.setInt(8, tid);
            ps.setDouble(9, OBTAIN_MARK);
            ps.setDouble(10, TotalMark);
            ps.setInt(11, Phy_correct_Questions);
            ps.setInt(12,Chem_correct_Questions );
            ps.setInt(13,Bio_correct_Questions );
            ps.setInt(14,Phy_incorrect_Questions );
            ps.setInt(15,Chem_incorrect_Questions );
            ps.setInt(16,Bio_incorrect_Questions);
            ps.setInt(17, TOTAL_PHY_QUESTIONS);
            ps.setInt(18,TOTAL_CHEM_QUESTIONS );
            ps.setInt(19,TOTAL_Bio_QUESTIONS );
            ps.setDouble(20,TOTAL_PHY_MARK );
            ps.setDouble(21,TOTAL_CHEM_MARK );
            ps.setDouble(22,TOTAL_Bio_MARK);
            ps.setString(23,classTestBean.getAcademicYear());
            ps.setString(24,classTestBean.getClass_Std());
            ps.setString(25,classTestBean.getDivission());
            int r = ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void addResult(UnitTestResultBean unitTestResultBean, int rollNo) {
        if (unitTestResultBean != null) {
            Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
            try {
                Statement s1 = con.createStatement();
                s1.executeUpdate("update CurrentTestStatusOfStudent set statusoftest=1 where rollno=" + rollNo);
                String sql = "insert into UTest_Result_Info values(?,?,?,?)";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setInt(1, unitTestResultBean.getUnitTsetId());
                ps.setInt(2, rollNo);
                ps.setInt(3, unitTestResultBean.getTotalQuestions());
                ps.setDouble(4, unitTestResultBean.getCorrectQuestions());

                int r = ps.executeUpdate();
                if (r > 0) {
                    //System.out.println("Practice Result Added");
                } else {
                    //System.out.println("Problem in Result Adding");
                }
            } catch (Exception ex) {
                //JOptionPane.showMessageDialog(null, ex.getMessage());
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void setClassQuestionStatus(QuestionBean question, int unitTestId, String userAnswer,int Rollno) {
        if (question != null) {
            Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = con.createStatement();
                s.executeUpdate("update Class_Test_Question_Status_Info set user_Answer='" + userAnswer + "' , SUBJECT_ID ="+question.getSubjectId()+" where question_Id=" + question.getQuestionId() + " and test_Id=" + unitTestId+ "and ROLLNO="+Rollno);
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public String getResult1(int unitTestId, int RollNo,int Questionid) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select user_Answer from  Class_Test_Question_Status_Info where test_Id=" + unitTestId + " and ROLLNO=" + RollNo + "and QUESTION_ID="+Questionid);
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public void saveStateOfNewTest(UnitTestBean unitTestBean, int rollNo) {
        int testId = 0;
        if (unitTestBean != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = conn.createStatement();
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);
                ArrayList<QuestionBean> alPhysics = unitTestBean.getQuestions();

                Iterator<QuestionBean> it = alPhysics.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    PreparedStatement ps = conn.prepareStatement("insert into Class_Test_Question_Status_Info values(?,?,?,?,?)");
                    ps.setInt(1, unitTestBean.getUnitTestId());
                    ps.setInt(2, rollNo);
                    ps.setInt(3, question.getQuestionId());
                    ps.setString(4, "unAttempted");
                    ps.setInt(5, question.getSubjectId());
                    ps.executeUpdate();
                }
                unitTestBean.setStartDateTime(new java.util.Date());
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public int InsertTestDetails(ArrayList<QuestionBean> questions, int rollNo) throws IOException {
        int ret = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement ss = conn.createStatement();
            ResultSet rrr = ss.executeQuery("select Max(TestBeanId) from StudentTestBean");
            if (rrr.next()) {
                ret = rrr.getInt(1);
            }
            ret++;
            PreparedStatement ps = conn.prepareStatement("insert into StudentTestBean values(?,?,?)");
            ps.setInt(1, ret);
            ps.setInt(2, rollNo);
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
            objOutStream.writeObject(questions);
            objOutStream.flush();
            objOutStream.close();
            byteOutStream.close();
            ps.setBytes(3, byteOutStream.toByteArray());
            ps.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }

    public String getNameOfStudent(int rollNo) {
        String name = "";
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());

        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("select fullname from Student_Info where rollno=" + rollNo);
            if (rs.next()) {
                name = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }
    
     public int getPhyCount(int Subjectid,int unitTestId) {
         int phyQCount=0;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select count(*) from  Class_Test_Question_Info where SUBJECT_ID=" + Subjectid + " and TESTID=" +unitTestId);
            if (rs.next()) {
                phyQCount= rs.getInt(1);
                return phyQCount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return phyQCount;
    }
     
     public int getChemCount(int Subjectid,int unitTestId) {
         int phyQCount=0;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select count(*) from  Class_Test_Question_Info where SUBJECT_ID=" + Subjectid + " and TESTID=" +unitTestId);
            if (rs.next()) {
                phyQCount= rs.getInt(1);
                return phyQCount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return phyQCount;
    }
     
     public int getBioCount(int Subjectid,int unitTestId) {
         int phyQCount=0;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select count(*) from  Class_Test_Question_Info where SUBJECT_ID=" + Subjectid + " and TESTID=" +unitTestId);
            if (rs.next()) {
                phyQCount= rs.getInt(1);
                return phyQCount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return phyQCount;
    }
public double getPerQuestionMarks1(int subjectId) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select * from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(3);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

  public double getPerwrongQuestionMarks1(int subjectId) {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
  
  public ArrayList<RankBean> calculateRank(int TestID) {
        con =  new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<RankBean> rb = new ArrayList<RankBean>();
        try {
            s = con.createStatement();
            rs = s.executeQuery("Select * from Class_Test_Result_Info where test_Id=" + TestID);
            while (rs.next()) {
                RankBean rr = new RankBean();
                rr.setUnitTestId(rs.getInt(1));
                rr.setRollNo(rs.getInt(2));
                rr.setSubject_Id(rs.getInt(3));
                rr.setTotal_Questions(rs.getInt(4));
                rr.setCorrect_Questions((int) rs.getInt(5));
                rr.setIncorrect_Questions((int) rs.getInt(6));
                rr.setTestbeanId(rs.getInt(8));
                rr.setObtainMark(rs.getDouble(9));
                rr.setTotalMark(rs.getDouble(10));
                rr.setPHY_CORRECT_QUESTIONS(rs.getDouble(11));
                rr.setCHEM_CORRECT_QUESTIONS(rs.getDouble(12));
                rr.setMATH_CORRECT_QUESTIONS(rs.getDouble(13));
                rr.setPHY_INCORRECT_QUESTIONS(rs.getDouble(14));
                rr.setCHEM_INCORRECT_QUESTIONS(rs.getDouble(15));
                rr.setMATH_INCORRECT_QUESTIONS(rs.getDouble(16));
                rr.setTOTAL_PHY_QUESTIONS(rs.getDouble(17));
                rr.setTOTAL_CHEM_QUESTIONS(rs.getDouble(18));
                rr.setTOTAL_MATH_QUESTIONS(rs.getDouble(19));
                rr.setTOTAL_PHY_MARK(rs.getDouble(20));
                rr.setTOTAL_CHEM_MARK(rs.getDouble(21));
                rr.setTOTAL_MATH_MARK(rs.getDouble(22));
                Timestamp timeStamp = rs.getTimestamp(7);
                java.sql.Date date = new java.sql.Date(timeStamp.getTime());
                String str = date.toString();
                rr.setTimeinstring(str);
                rb.add(rr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(JEE.unitTest.DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return rb;
    }
    
    public ArrayList<QuestionBean> getTestDetails(int rollNo, int testBeanId) {
        ArrayList<QuestionBean> ret = new ArrayList<QuestionBean>();
        con =new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            String sql = "select * from StudentTestBean where TestBeanId=? and rollno=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, testBeanId);
            ps.setInt(2, rollNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                byte[] data = rs.getBytes(3);
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                ret = (ArrayList<QuestionBean>) obj;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(JEE.unitTest.DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }

    
    public int changeStatusOfTest(String string, int flag, int testid) {
        int i = 0;
        con = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        
        try {
            s= con.createStatement();
            s.executeUpdate("update Class_Test_Info set status='" + string + "' where test_Id=" + testid);
            i = flag;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return i;
    }
    public String getPatternName(int TestID)
  {
      String PatternName=null;
      con =  new DBConnection1().getClientConnection1(Server.getServerIP());
      try {
            s = con.createStatement();
            rs = s.executeQuery("Select Pattern_Name from  Class_Test_Info where test_Id = " + TestID);
            while (rs.next()) {
                PatternName=rs.getString(1);
                return PatternName;
            }
         } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }    
      return PatternName;
  }  
    
     public void clearResponseOption(int testId,int Rollno,QuestionBean question) {
      
             con = new DBConnection1().getClientConnection1(Server.getServerIP());
           
            try {
                s = con.createStatement();
              
                s.executeUpdate("update CLASS_TEST_QUESTION_STATUS_INFO set user_Answer='unAttempted' where TEST_ID="+testId+" and ROLLNO="+ Rollno+" and  QUESTION_ID="+question.getQuestionId()+" ");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
}