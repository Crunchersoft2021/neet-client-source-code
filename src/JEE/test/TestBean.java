package JEE.test;
import com.bean.QuestionBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;



public class TestBean implements Serializable {
    private int testId,groupId;
    private Date startDateTime;
    private ArrayList<QuestionBean> alPhysics,alChemistry,alMaths,alUnitTest;
    private String status;
    private long remainingTime;
    private int totalTime;
    private int currentSubjectNumber;

    public ArrayList<QuestionBean> getAlMaths() {
        return alMaths;
    }

    public void setAlMaths(ArrayList<QuestionBean> alMaths) {
        this.alMaths = alMaths;
    }
    private int currentQuestionNumber;

    public int getCurrentQuestionNumber() {
        return currentQuestionNumber;
    }

    public void setAlUnitTest(ArrayList<QuestionBean> alUnitTest) {
        this.alUnitTest = alUnitTest;
    }

    public void setCurrentQuestionNumber(int currentQuestionNumber) {
        this.currentQuestionNumber = currentQuestionNumber;
    }

    public ArrayList<QuestionBean> getAlUnitTest() {
        return alUnitTest;
    }

    public int getCurrentSubjectNumber() {
        return currentSubjectNumber;
    }

    public void setCurrentSubjectNumber(int currentSubjectNumber) {
        this.currentSubjectNumber = currentSubjectNumber;
    }
    
    public long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public ArrayList<QuestionBean> getAlChemistry() {
        return alChemistry;
    }

    public void setAlChemistry(ArrayList<QuestionBean> alChemistry) {
        this.alChemistry = alChemistry;
    }

    public ArrayList<QuestionBean> getAlPhysics() {
        return alPhysics;
    }

    public void setAlPhysics(ArrayList<QuestionBean> alPhysics) {
        this.alPhysics = alPhysics;
    }
}