/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 007
 */
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import server.DBConnection1;

public class EmployeeInit {

    public void insertImageInDB(String path) {
        try {
            Connection con;
            String halfurl = new DBConnection1().getURL(server.Server.getServerIP());
            con = DriverManager.getConnection(halfurl + "c:\\db\\employee");
            PreparedStatement ps;
            ps = con.prepareStatement("insert into AllImages(ImageName,ImageObject) " + "values(?,?)");
            ps.setString(1, path);

            Blob blob = con.createBlob();
            ImageIcon ii = new ImageIcon(path);

            ObjectOutputStream oos;
            oos = new ObjectOutputStream(blob.setBinaryStream(1));
            oos.writeObject(ii);
            oos.close();
            ps.setBlob(2, blob);
            ps.execute();
            blob.free();
            ps.close();
        } catch (IOException ex) {
            Logger.getLogger(EmployeeInit.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeInit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws Exception {
//        Connection con;
//        String halfurl = new DBConnection1().getURL(server.Server.getServerIP());
//        con = DriverManager.getConnection(halfurl + "c:\\db\\employee");
//
//        PreparedStatement ps;
//        ps = con.prepareStatement("insert into employee(name,photo) " + "values(?,?)");
//        ps.setString(1, "Duke");
//
//        Blob blob = con.createBlob();
//        ImageIcon ii = new ImageIcon("duke.png");
//
//        ObjectOutputStream oos;
//        oos = new ObjectOutputStream(blob.setBinaryStream(1));
//        oos.writeObject(ii);
//        oos.close();
//        ps.setBlob(2, blob);
//        ps.execute();
//        blob.free();
//        ps.close();
    }
}